const express = require('express')
const cors = require('cors')
const { spawn, exec } = require('child_process');
Stream = require('node-rtsp-stream')

const app = express()
app.use(cors())


let processes = {}
// the 3 cameras avaialble
const camerasDictionary = {
	MONITOR_CAMERA: '1',
	DUMBBELL_MACHINE_CAMERA: '2',
	INJECTORS: '3'
}
const rtspStream = {
	[camerasDictionary.MONITOR_CAMERA]: "EU40.cast-tv.com:1935/23595_LIVE_Kotel_LIVE2/23595_LIVE_Kotel_LIVE2",
	[camerasDictionary.DUMBBELL_MACHINE_CAMERA]: "EU40.cast-tv.com:1935/23595_LIVE_Kotel_Live1/23595_LIVE_Kotel_Live1",
	[camerasDictionary.INJECTORS]: "freja.hiof.no:1935/rtplive/definst/hessdalen03.stream"
}
const cameras = {
	[camerasDictionary.MONITOR_CAMERA]: null,
	[camerasDictionary.DUMBBELL_MACHINE_CAMERA]: null,
	[camerasDictionary.INJECTORS]: null
}
// the 3 cameras ports.
const camerasWSPort = {
	[camerasDictionary.MONITOR_CAMERA]: 9991,
	[camerasDictionary.DUMBBELL_MACHINE_CAMERA]: 9992,
	[camerasDictionary.INJECTORS]: 9993
}
// params : cameraName (cameras KEY) ipAdrr (the ip of the camera you want to connect to on this specific ws port)
const startRTSPStreaming = (cameraName, ipAddr) => {

	if (cameras[cameraName]) {
		disconnectRTSPStreaming(cameraName)
	}

	cameras[cameraName] = new Stream({
		name: `RTSPCamera-${cameraName}`,
		// streamUrl: `rtsp://admin:123456@${ipAddr}:554`,
		streamUrl: `rtsp://${rtspStream[cameraName]}`,
		wsPort: camerasWSPort[cameraName],
		ffmpegOptions: { // options ffmpeg flags
			'-stats': '', // an option with no neccessary value uses a blank string
			'-r': 30, // options with required values specify the value after the key
			'-b': '3000k', // bite rate , effects quality
			'-an': '', // we dont want any sound
		}
	})
}
// need to check what is the right command to kill the streaming proccess 
const disconnectRTSPStreaming = (cameraName) => {
	/**
	 * ----------------------
	 * kills websocket server and stream
	 * ----------------------
	 * VideoStream.prototype.stop = function() {
  		this.wsServer.close()
  		this.stream.kill()
  		this.inputStreamStarted = false
  		return this
	}
	 */
	if (cameras[cameraName]) {
		cameras[cameraName].stop();
		cameras[cameraName] = null;
	}

}
// if we got anything in there, its still running.
const isRTSPCameraConnected = (cameraName) => {
	return (cameras[cameraName] != null)
}

app.get('/connectRTSPCamera/:cameraName/:ipAddr', ({ params: { cameraName, ipAddr } }, res) => {
	startRTSPStreaming(cameraName, ipAddr)
	res.sendStatus(200)
})
app.get('/disconnectRTSPCamera/:cameraName', ({ params: { cameraName } }, res) => {
	disconnectRTSPStreaming(cameraName)
	res.sendStatus(200)
})
app.get('/rtspCameraStatus/:cameraName', ({ params: { cameraName } }, res) => {
	res.send(isRTSPCameraConnected(cameraName))
})

app.get('/disconnectCamera/:clientId/:cameraIp', ({ params: { clientId, cameraIp } }, res) => {
	if (processes[clientId]) {
		if (processes[clientId][cameraIp]) processes[clientId][cameraIp].kill("SIGINT")
	}
	res.sendStatus(200)
})



app.get('/connectCamera/:clientId/:cameraIp/:measures/:isExists', ({ params: { clientId, cameraIp, measures, isExists } }, res) => {
	if (processes[clientId]) {
		if (processes[clientId][cameraIp]) processes[clientId][cameraIp].kill("SIGINT")

	} else processes[clientId] = {}

	if (measures != "none") {
		processes[clientId][cameraIp] = spawn("python", ["C:/vitalmonitor/main.py", "--id", clientId, "--ip", cameraIp, "--exists", isExists, "--measures"].concat(measures.split(',')))
	} else {
		processes[clientId][cameraIp] = spawn("python", ["C:/vitalmonitor/main.py", "--id", clientId, "--ip", cameraIp, "--exists", isExists, "--measures"])
	}
	res.sendStatus(200)
})

app.listen(4000)