from pymongo import MongoClient
import os
import json
import datetime
from bson.json_util import dumps
os.chdir(os.path.dirname(__file__))

dbClient = MongoClient("mongodb://localhost:27017/")
db = dbClient["eyedoc"]
CalibrationData = db.get_collection("CalibrationData")

def getLastCalibrationData(cameraIp):
    return CalibrationData.find_one({"ip": cameraIp}, {"ip": 1, "_id": 0})

