import json
from flask import Blueprint, request, jsonify
from flask_cors import cross_origin
import src.Calibrations.CalibrationBL as CalibrationBL

calibrationController = Blueprint('calibrationController', __name__)

@calibrationController.route('/isCalibrationSaved/<cameraIp>')
@cross_origin(origin="*")
def isCalibrationSaved(cameraIp):
    return jsonify(CalibrationBL.isCalibrationExists(cameraIp))
