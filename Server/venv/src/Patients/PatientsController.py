import json
from flask import Blueprint, request, jsonify, send_file
from flask_cors import cross_origin
from src.Users.UsersBL import isAdmin
from bson.json_util import dumps
import src.Patients.PatientsBL as PatientsBL

patientController = Blueprint('patientController', __name__)


@patientController.route('/newPatient', methods=['POST'])
@cross_origin(origin="*")
def new_patient():
    firstName = request.get_json()['firstName']
    lastName = request.get_json()['lastName']
    age = request.get_json()['age']
    bedId = request.get_json()['bedId']
    patientId = request.get_json()['id']
    userId = request.get_json()['userId']
    message = {}

    if (not isAdmin(userId)):
        return jsonify({"error": "No permission"})
    patient = PatientsBL.add_newPatient({"firstName": firstName, "lastName": lastName, "age": age, "id": patientId,
                                          "bedId": bedId, "severityData": PatientsBL.defaultSeverityData, "isDeleted": False})
    if patient != False:
        message = {"message": "המטופל " + firstName + " " + lastName + " נוסף בהצלחה"}
    else:
        message = {"message": "מספר מיטה " + bedId + " כבר קיים במערכת"}

    return jsonify(message)


@patientController.route('/editPatient', methods=['POST'])
@cross_origin(origin="*")
def edit_patient():
    bedId = request.get_json()['bedId']
    firstName = request.get_json()['firstName']
    lastName = request.get_json()['lastName']
    age = request.get_json()['age']
    patientId = request.get_json()['id']
    severityData = request.get_json()['severityData']
    userId = request.get_json()['userId']

    if (not isAdmin(userId)):
        return jsonify({"error": "No permission"})
    PatientsBL.editPatient(bedId, {"firstName": firstName, "lastName": lastName, "age": age,
                     "id": patientId, "bedId": bedId, "severityData": severityData, "isDeleted": False})

    return jsonify({"message": "מיטה " + bedId + " עודכנה בהצלחה"})


@patientController.route('/deletePatient', methods=['POST'])
@cross_origin(origin="*")
def delete_patient():
    identifier = request.get_json()['identifier']
    firstName = request.get_json()['firstName']
    lastName = request.get_json()['lastName']
    userId = request.get_json()['userId']

    if (not isAdmin(userId)):
        return jsonify({"error": "No permission"})

    PatientsBL.deletePatient(identifier)

    return jsonify({"message": "המוטפל " + firstName + " " + lastName + " נמחק מהמערכת"})


@patientController.route('/patients')
@cross_origin(origin="*")
def get_patients():
    id = request.args.get('auth')
    if (id == "1" or id == "2"):
        return jsonify({"patients": dumps(PatientsBL.get_allPatients())})
    else:
        # do we want to do something else here , like send a string or w/e so he wouldent even see the screen ?
        return jsonify({"patients": []})