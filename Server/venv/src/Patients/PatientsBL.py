import json
import src.Patients.PatientsDal as PatientsDal

def get_allPatients():
    return PatientsDal.get_allPatients()

def add_newPatient(patient):
    if PatientsDal.get_patientByBedId(patient["bedId"]) is None:
        return PatientsDal.add_newPatient(patient)
    else: return False

def editPatient(bedId, edittedPatient):
    PatientsDal.editPatient(bedId, edittedPatient)

def deletePatient(bedId):
    PatientsDal.deletePatient(bedId)
    
defaultSeverityData = {
    "minSBP": 90,
    "mediumSBP" : 120,
    "maxSBP": 140,
    "minDBP" : 60,
    "mediumDBP" : 80,
    "maxDBP" : 90,
    "maxHRV":120,
    "minHRV":40,
    "maxPR":72,
    "minPR":36,
    "minCO2": 20,
    "maxTEMP": 38.3,
    "minTEMP": 35,
    "mediumTEMP": 37.5,
    "minSAT": 94,
    "mediumBR": 20,
    "maxBR": 30
}