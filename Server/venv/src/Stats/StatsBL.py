from src.Cameras.CamerasBL import camerasType
import src.Cameras.CamerasDal as CamerasDal
import src.Stats.StatsDal as StatsDal
import os
from src.Patients.PatientsBL import get_allPatients
os.chdir(os.path.dirname(__file__))


def get_lastMonitorData(bedId, camera_ip):
    return StatsDal.get_lastMonitorData(bedId, camera_ip)


def get_lastMonitorDataByBedId(bedId):
    data = []
    camerasIp = CamerasDal.get_camerasIp_byBedId(bedId)
    for ip in camerasIp:
        datum = StatsDal.get_lastMonitorData(bedId, ip["ip"])
        if datum is not None:
            data.append(datum["stats"])
    return data


def get_measureData(bedId, cameraTargetId, measure):
    measureList = []
    cameraIp = CamerasDal.get_cameraIp(bedId, camerasType[cameraTargetId])
    if cameraIp is not None:
        measureList = StatsDal.get_measureData(bedId, cameraIp, measure)
    return measureList


def get_measureDataByTime(bedId, cameraTargetId, measure, minutes):
    measureList = []
    cameraIp = CamerasDal.get_cameraIp(bedId, camerasType[cameraTargetId])
    if cameraIp is not None:
        measureList = StatsDal.get_measureDataByTime(
            bedId, cameraIp, measure, minutes)
    return measureList


def get_BPLevel(SBPValue, DBPValue, severityData):
    if ((SBPValue > severityData["maxSBP"] and DBPValue > severityData["maxDBP"]) or
            (SBPValue < severityData["minSBP"] and DBPValue < severityData["minSBP"])):
        return dataLevelTypes["HIGH"]

    if((SBPValue < severityData["maxSBP"] and SBPValue > severityData["mediumSBP"] and DBPValue < severityData["maxDBP"] and DBPValue > severityData["mediumDBP"])):
        return dataLevelTypes["MEDIUM"]

    return dataLevelTypes["NORMAL"]


def get_HRVLevel(hrvValue, severityData):
    if hrvValue < severityData["minHRV"] or hrvValue > severityData["maxHRV"]:
        return dataLevelTypes["HIGH"]

    return dataLevelTypes["NORMAL"]


def get_PRLevel(PRValue, severityData):
    if PRValue < severityData["minPR"] or PRValue > severityData["maxPR"]:
        return dataLevelTypes["HIGH"]

    return dataLevelTypes["NORMAL"]


def get_CO2Level(CO2Value, severityData):
    if CO2Value < severityData["minCO2"]:
        return dataLevelTypes["HIGH"]

    return dataLevelTypes["NORMAL"]


def get_TEMPLevel(TEMPValue, severityData):

    if (TEMPValue > 0 and TEMPValue < severityData["minTEMP"]) or TEMPValue > severityData["maxTEMP"]:
        return dataLevelTypes["HIGH"]

    if TEMPValue > severityData["mediumTEMP"]:
        return dataLevelTypes["MEDIUM"]

    return dataLevelTypes["NORMAL"]


def get_SATLevel(SATValue, severityData):
    if SATValue < severityData["minSAT"]:
        return dataLevelTypes["HIGH"]

    return dataLevelTypes["NORMAL"]


def get_BRLevel(BRValue, age, severityData):
    if age > 16:
        if BRValue > severityData["maxBR"]:
            return dataLevelTypes["HIGH"]
        if BRValue >= severityData["mediumBR"] and BRValue <= severityData["maxBR"]:
            return dataLevelTypes["MEDIUM"]
    return dataLevelTypes["NORMAL"]


def get_severityLevel(patientStats, patientSeverityData, age):
    severityStruct = {
        "level": dataLevelTypes["NORMAL"],
        "reason": reasons["NORMAL"]
    }
    if(not check_criticalLevel(patientStats, age, patientSeverityData, severityStruct)):
        if(not check_highLevel(patientStats, age, patientSeverityData, severityStruct)):
            check_mediumLevel(patientStats, age,
                              patientSeverityData, severityStruct)

    return severityStruct


def check_highLevel(stats, age, severityData, severityStruct):
    if(
        stats["blood_pressure"]["value"][0] < severityData["minSBP"]
    ):
        severityStruct["reason"] = reasons["SBP_LOW"]
    elif(
        stats["saturation"]["value"] < severityData["minSAT"]
    ):
        severityStruct["reason"] = reasons["SAT_LOW"]

    if(severityStruct["reason"] != reasons["NORMAL"]):
        severityStruct["level"] = dataLevelTypes["HIGH"]

    return severityStruct["level"] == dataLevelTypes["HIGH"]


def check_criticalLevel(stats, age, severityData, severityStruct):
    if(
        stats["blood_pressure"]["value"][0] < severityData["minSBP"]
        and
        stats["saturation"]["value"] < severityData["minSAT"]
    ):
        severityStruct["reason"] = reasons["SBP_LOW_SAT_LOW"]
    elif(
        stats["saturation"]["value"] < severityData["minSAT"]
        and
        stats["breath_rate"]["value"] > severityData["mediumBR"]
        and
        (stats["pulse"]["value"] > severityData["maxHRV"]
         or stats["pulse"]["value"] < severityData["minHRV"])
    ):
        severityStruct["reason"] = reasons["SAT_LOW_BR_HIGH_HRV_OUT"]
    elif(
        stats["blood_pressure"]["value"][0] < severityData["minSBP"]
        and
        (stats["pulse"]["value"] > severityData["maxHRV"]
         or stats["pulse"]["value"] < severityData["minHRV"])
    ):
        severityStruct["reason"] = reasons["SBP_LOW_HRV_OUT"];
    elif(
        stats["blood_pressure"]["value"][0] < severityData["minSBP"]
        and
        stats["breath_rate"]["value"] > severityData["mediumBR"]
    ):
        severityStruct["reason"] = reasons["BR_HIGH_SBP_LOW"]

    if(severityStruct["reason"] != reasons["NORMAL"]):
        severityStruct["level"] = dataLevelTypes["CRITICAL"]

    return severityStruct["level"] == dataLevelTypes["CRITICAL"]


def check_mediumLevel(stats, age, severityData, severityStruct):
    if(
        stats["breath_rate"]["value"] > severityData["mediumBR"]
    ):
        severityStruct["reason"] = reasons["BR_HIGH"]
    elif(
        stats["pulse"]["value"] > severityData["maxHRV"] or stats["pulse"]["value"] < severityData["minHRV"]
    ):
        severityStruct["reason"] = reasons["HRV_OUT"]

    if(severityStruct["reason"] != reasons["NORMAL"]):
        severityStruct["level"] = dataLevelTypes["MEDIUM"]

    return severityStruct["level"] == dataLevelTypes["MEDIUM"]


# OUT = out of range
reasons = {
    # "ALL_CRITICAL": 0,
    # "LOW_SETORATION_LOW_BP": 1,
    # "HIGH_PULSE": 2,
    # "LOW_PULSE": 3,
    # "BREATH_RATE_HIGH_SETORATION_LOW": 4,
    # "BREATH_RATE_LOW_SETORATION_LOW": 5,
    # "NORMAL": 6,
    "NORMAL": 0,
    "SBP_LOW_SAT_LOW": 1,
    "SAT_LOW_BR_HIGH_HRV_OUT": 2,
    "SBP_LOW_HRV_OUT": 3,
    "BR_HIGH_SBP_LOW": 4,
    "SBP_LOW" : 5,
    "SAT_LOW" : 6,
    "BR_HIGH" : 7,
    "HRV_OUT" : 8



}

dataType = {
    "BREATH_RATE": 'breath rate',
    "PULSE": 'pulse',
    "blood_pressure": 'blood pressure',
    "SATURATION": 'saturation',
    "AVERAGE_PRESSURE": 'averagePressure',
}

dataLevelTypes = {
    "CRITICAL": 'critical',
    "HIGH": 'high',
    "MEDIUM": 'medium',
    "NORMAL": 'normal'
}
