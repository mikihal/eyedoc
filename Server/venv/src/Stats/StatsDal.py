from pymongo import MongoClient
import os
import json
import datetime
from bson.json_util import dumps

os.chdir(os.path.dirname(__file__))

dbClient = MongoClient("mongodb://localhost:27017/")
db = dbClient["eyedoc"]
MonitorData = db.get_collection("MonitorData")


# Stats
def get_lastMonitorData(bedId, cameraIp):
    return MonitorData.find_one({"cameraIp": cameraIp, "bedId": bedId,
                                 "datetime": {"$gt": datetime.datetime.utcnow() - datetime.timedelta(seconds=60)}},
                                sort=[("datetime", -1)])

#Analysis
def get_measureData(bedId, cameraIp, measure):
    if (measure == "blood_pressure"):
        value = {"$arrayElemAt": ["$stats." + measure, 0]}
    else:
        value = "$stats." + measure
    return MonitorData.aggregate([{"$project": {"stats." + measure: 1, "_id": 0,
                                                "datetime": 1, "bedId": 1, "cameraIp": 1}},
                                  {"$match": {"bedId": bedId, "cameraIp": cameraIp, "datetime": {
                                      "$gt": datetime.datetime.utcnow() - datetime.timedelta(minutes=3)}}},
                                  {"$project": {"value": value,
                                                "time": {"$subtract": ["$datetime", datetime.datetime(1970, 1, 1)]}}},
                                  {"$sort": {"datetime": 1}}])

#Analysis
def get_measureDataByTime(bedId, cameraIp, measure, minutes):
    timeByMinutes = int(minutes)
    if timeByMinutes > 120:
        timeByMinutes = 120

    if (measure == "blood_pressure"):
        value = {"$arrayElemAt": ["$stats." + measure, 0]}
    else:
        value = "$stats." + measure
    return MonitorData.aggregate([{"$project": {"stats." + measure: 1, "_id": 0,
                                                "datetime": 1, "bedId": 1, "cameraIp": 1}},
                                  {"$match": {"bedId": bedId, "cameraIp": cameraIp, "datetime": {
                                      "$gt": datetime.datetime.utcnow() - datetime.timedelta(minutes=timeByMinutes)}}},
                                  {"$project": {"value": value,
                                                "time": {
                                                    "$subtract": ["$datetime", datetime.datetime(1970, 1, 1)]}}},
                                  {"$sort": {"datetime": 1}}])
