import json
import src.Users.UsersDal as UsersDal


def isAdmin(id):
    result = False
    user = UsersDal.get_userById(id)
    result = user['admin']
    return result


def get_user(name, password):
    return UsersDal.get_user(name, password)
