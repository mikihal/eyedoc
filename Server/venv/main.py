import os
from flask_cors import CORS, cross_origin
from flask import Flask, send_file, jsonify, request

from src.Users.UsersController import usersController
from src.Patients.PatientsController import patientController
from src.Cameras.CamerasController import cameraController
from src.Stats.StatsController import statsController
from src.Calibrations.CalibrationController import calibrationController

os.chdir(os.path.dirname(__file__))

app = Flask(__name__, static_folder="static", static_url_path="/")
app.register_blueprint(usersController)
app.register_blueprint(patientController)
app.register_blueprint(cameraController)
app.register_blueprint(statsController)
app.register_blueprint(calibrationController)
CORS(app)


@app.route('/')
@cross_origin(origin="*")
def index():
    return send_file('static/index.html')


if __name__ == "__main__":
    app.run(debug=True)
