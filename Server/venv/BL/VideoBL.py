import json

camerasType = {
    1: "monitorCameraIp",
    2: "hanshamaCameraIp",
    3: "odMasheuCameraIp"
}

def get_cameraIp(clientId, targetId):
    with open(r'C:\eyedoc\Server\venv\Files\Cameras.json') as json_file:
        data = json.load(json_file)
        return data[clientId][camerasType[int(targetId)]]

def get_allPatients():
    with open(r'C:\eyedoc\Server\venv\Files\Patients.json', encoding="utf8") as patients_file:
        data = json.load(patients_file)
        return data



