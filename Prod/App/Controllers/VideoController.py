import os
import json
import sys
import threading
import base64
import codecs
import datetime
from flask_cors import CORS, cross_origin
from flask import Flask, send_file, jsonify, request
from flask_jwt_extended import JWTManager
from flask_jwt_extended import create_access_token
from enum import Enum
from subprocess import call
from functools import wraps
sys.path.insert(0, '../BL')
from VideoBL import get_allPatients
from VideoBL import get_cameraIp

app = Flask(__name__)
CORS(app)

app.config["JWT_SECRET_KEY"] = 'eyedoc'

jwt = JWTManager(app)

def runCameraConfig(clientId, camera_ip):
    call(['python', 'C:/vitalmonitor/main.py', clientId, camera_ip])


@app.route('/connectCamera/<clientId>/<cameraTargetId>')
@cross_origin(origin="*")
def connect_camera(clientId, cameraTargetId):
    camera_ip = get_cameraIp(clientId, cameraTargetId)
    threading.Thread(target=runCameraConfig,
                     args=(clientId, camera_ip,)).start()
    return jsonify(success=True)


@app.route('/image/<clientId>/<cameraTargetId>')
@cross_origin(origin="*")
def get_image(clientId, cameraTargetId):
    camera_ip = get_cameraIp(clientId, cameraTargetId)
    image_dir = r"C:\vitalmonitor/Data/" + \
        clientId + "/" + camera_ip + "/im_out.jpg"
    if os.path.exists(image_dir):
        data = base64.b64encode(open(image_dir, 'rb').read()).decode('utf-8')
    else:
        data = jsonify([])

    return data


@app.route('/stats/<clientId>/<cameraTargetId>')
@cross_origin(origin="*")
def get_stats(clientId, cameraTargetId):
    camera_ip = get_cameraIp(clientId, cameraTargetId)
    stats_dir = r"C:\vitalmonitor/Data/" + \
        clientId + "/" + camera_ip + "/data_out.json"
    if os.path.exists(stats_dir):
        with open(stats_dir, "r") as file:
            statsData = json.load(file)
            jsonToReturn = []
            for stat in statsData["stats"]:
                jsonToReturn.append({"name": stat, "value": statsData["stats"][stat]})
            return jsonify(jsonToReturn)
    else:
        return jsonify([])

@app.route('/allStats')
@cross_origin(origin="*")
def get_all_stats():
    #stats_dir = r"C:\vitalmonitor/Data/" + clientId + "/" + camera_ip + "/data_out.json"
    allStatsJson = []
    for root, dirs, files in os.walk(r"C:\vitalmonitor/Data/"):
        for name in files:
            if name == "data_out.json":
                with open(os.path.join(root, name), 'r') as statsFile:
                    allStatsJson.append(json.load(statsFile))
    return jsonify(allStatsJson)


@app.route('/newPatient', methods=['POST'])
@cross_origin(origin="*")
def new_patient():
    firstName = request.get_json()['firstName']
    lastName = request.get_json()['lastName']
    age = request.get_json()['age']
    bedId = request.get_json()['bedId']

    with open(r'C:\eyedoc\Server\venv\Files\Patients.json', encoding="utf8") as clients_file:
        data = json.load(clients_file)

    patients = data["patients"]
    patients.append({"firstName": firstName, "lastName": lastName, "age": age, "id": bedId, "bedId": bedId, "severityData": defaultSeverityData})

    with open(r'C:\eyedoc\Server\venv\Files\Patients.json', "w", encoding="utf8") as new_patients_file:
        json.dump({"patients": patients}, new_patients_file, ensure_ascii=False)

    return jsonify({"message": "המוטפל " + firstName + " "+ lastName +" נוסף בהצלחה"})


@app.route('/editPatient', methods=['POST'])
@cross_origin(origin="*")
def edit_patient():

    identifier = request.get_json()['identifier']
    firstName = request.get_json()['firstName']
    lastName = request.get_json()['lastName']
    age = request.get_json()['age']
    bedId = request.get_json()['bedId']

    with open(r'C:\eyedoc\Server\venv\Files\Patients.json', encoding="utf8") as clients_file:
        data = json.load(clients_file)

    patients = data["patients"]
    patientIndexToDelete = None

    for i in range(len(patients)):
        if patients[i]["id"] == identifier:
            patientIndexToDelete = i

    if(patientIndexToDelete != None):
        del patients[patientIndexToDelete]

    patients.append({"firstName": firstName, "lastName": lastName, "age": age, "id": bedId, "bedId": bedId, "severityData": defaultSeverityData})
    with open(r'C:\eyedoc\Server\venv\Files\Patients.json', "w", encoding="utf8") as new_patients_file:
        json.dump({"patients": patients}, new_patients_file, ensure_ascii=False)

    return jsonify({"message": "המוטפל " + firstName + " "+ lastName +" עודכן בהצלחה"})

@app.route('/deletePatient', methods=['POST'])
@cross_origin(origin="*")
def delete_patient():

    identifier = request.get_json()['identifier']
    firstName = request.get_json()['firstName']
    lastName = request.get_json()['lastName']

    with open(r'C:\eyedoc\Server\venv\Files\Patients.json', encoding="utf8") as clients_file:
        data = json.load(clients_file)

    patients = data["patients"]
    patientIndexToDelete = None

    for i in range(len(patients)):
        if patients[i]["id"] == identifier:
            patientIndexToDelete = i

    if(patientIndexToDelete != None):
        del patients[patientIndexToDelete]

    with open(r'C:\eyedoc\Server\venv\Files\Patients.json', "w", encoding="utf8") as new_patients_file:
        json.dump({"patients": patients}, new_patients_file, ensure_ascii=False)

    return jsonify({"message": "המוטפל " + firstName + " "+ lastName +" נמחק מהמערכת"})


@app.route('/patients')
@cross_origin(origin="*")
def get_patients():
    return get_allPatients()

@app.route('/login', methods=['POST'])
def login():
    id = request.get_json()['id']
    password = request.get_json()['password']
    result = ""

    if (password == "12345" and id == "admin") or (password == "laniado" and id == "laniado"):
        result = jsonify({'token': 'accepted'})
    else:
        result = jsonify({'error': "Invalid username and password"})

    return result

defaultSeverityData = {
                "grownPerson": 16,
                "lowPulse": 40,
                "highPulse": 120,
                "lowBlood": 85,
                "mediumBlood": 90,
                "lowBreath": 8,
                "mediumBreath": 20,
                "highBreath": 30,
                "lowAveragePressure": 60,
                "lowSturation": 85,
                "mediumSturation": 90
            }
app.run()
