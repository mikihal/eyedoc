import json
from flask import Blueprint, request, jsonify
from flask_cors import cross_origin
import src.Users.UsersBL as usersBl

usersController = Blueprint('usersController', __name__)

@usersController.route('/login', methods=['POST'])
@cross_origin(origin="*")
def login():

    name = request.get_json()['name']
    password = request.get_json()['password']
    result = jsonify({'error': "Invalid username and password"})

    user = usersBl.get_user(name, password)

    if user != None:
        result = jsonify({'token': user['id']})

    return result
