from pymongo import MongoClient
import os
os.chdir(os.path.dirname(__file__))

dbClient = MongoClient("mongodb://localhost:27017/")
db = dbClient["eyedoc"]
Users = db.get_collection("Users")

#Users
def get_user(name, password):
    return Users.find_one({"name": name, "password": password})

#Users
def get_userById(Id):
    return Users.find_one({"id": Id})
