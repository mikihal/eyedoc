from pymongo import MongoClient
import os
import json
import datetime
from bson.json_util import dumps
os.chdir(os.path.dirname(__file__))

dbClient = MongoClient("mongodb://localhost:27017/")
db = dbClient["eyedoc"]
Cameras = db.get_collection("Cameras")


#Cameras
def get_cameraIp(bedId, type):
    result = Cameras.find_one({"bedId": bedId, "type": type, "isDeleted": False})
    if result is not None:
        return result["ip"]
    else: return None

def get_lastCameraIp(bedId, type):
    return Cameras.find_one({"bedId": bedId, "type":type }, {"ip": 1}, sort=[("calibTime", -1)])

#Cameras
def delete_camera(bedId,type):
    return Cameras.update_one({"bedId": bedId, "type": type, "isDeleted": False}, {"$set":{"isDeleted": True}})

#Cameras
def add_cameraIP(bedId,type,cameraIP):
    Cameras.insert_one({"ip":cameraIP, "type":type,"bedId":bedId, "calibTime": datetime.datetime.utcnow() , "isDeleted": False})


#Cameras
def get_camerasIp_byBedId(bedId):
    return Cameras.find({"bedId": bedId, "isDeleted": False}, {"ip": 1})
