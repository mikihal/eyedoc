import shutil
import requests
from flask import Blueprint, request, jsonify, send_file
from flask_cors import cross_origin
import src.Cameras.CamerasBL as CamerasBL
cameraController = Blueprint('cameraController', __name__)

@cameraController.route('/getIP/<bedId>/<cameraTargetId>')
@cross_origin(origin="*")
def getIP(bedId, cameraTargetId):
    ip = CamerasBL.get_lastCameraIp(bedId, cameraTargetId)
    if ip is not None:
        return ip
    else: return jsonify("")



@cameraController.route('/connectCamera/<clientId>/<camera_ip>/<cameraTargetId>/<isExists>')
@cross_origin(origin="*")
def connect_camera(clientId, camera_ip, cameraTargetId, isExists):
    CamerasBL.add_cameraIP(clientId, cameraTargetId, camera_ip)
    measures = CamerasBL.get_measures(cameraTargetId)
    requests.get("http://localhost:4000/connectCamera/" +
                 clientId + "/" + camera_ip + "/" + measures + "/" + isExists)
    return jsonify(success=True)


@cameraController.route('/deleteCamera/<clientId>/<cameraTargetId>')
@cross_origin(origin="*")
def delete_camera(clientId, cameraTargetId):
    ip = CamerasBL.get_cameraIp(clientId, cameraTargetId)
    if ip is not None:
        CamerasBL.delete_camera(clientId, cameraTargetId)
        requests.get("http://localhost:4000/disconnectCamera/" +
                     clientId + "/" + ip)
    return jsonify(success=True)


@cameraController.route('/rtsp/connect/<bedId>/<cameraTargetId>')
@cross_origin(origin="*")
def connectRTSPCamera(bedId, cameraTargetId):
    cameraIp = CamerasBL.get_cameraIp(bedId, cameraTargetId)
    if (cameraIp):
        requests.get("http://localhost:4000/connectRtspCamera/" +
                     cameraTargetId + "/" + cameraIp)

    return jsonify(success=True)


@cameraController.route('/rtsp/disconnect/<cameraTargetId>')
@cross_origin(origin="*")
def disconnectRTSPCamera(cameraTargetId):
    requests.get(
        "http://localhost:4000/disconnectRTSPCamera/" + cameraTargetId)

    return jsonify(success=True)


@cameraController.route('/rtsp/status/<cameraTargetId>')
@cross_origin(origin="*")
def cameraStatus(cameraTargetId):
    result = requests.get(
        "http://localhost:4000/rtspCameraStatus/" + cameraTargetId)

    return jsonify(result.text == "true")


@cameraController.route('/rtsp/changeIp/<bedId>/<cameraTargetId>/<newIp>')
@cross_origin(origin="*")
def changeIp(bedId, cameraTargetId, newIp):
    CamerasBL.add_cameraIP(bedId,cameraTargetId, newIp)
    return jsonify(success=True)


@cameraController.route('/rtsp/disconnectAll')
@cross_origin(origin="*")
def disconnectAll():
    requests.get("http://localhost:4000/disconnectRTSPCamera/1")
    requests.get("http://localhost:4000/disconnectRTSPCamera/2")
    requests.get("http://localhost:4000/disconnectRTSPCamera/3")
    return jsonify(success=True)


@cameraController.route('/rtsp/connectAll/<bedId>')
@cross_origin(origin="*")
def connectAll(bedId):
    cameraIp1 = CamerasBL.get_cameraIp(bedId, "1")
    cameraIp2 = CamerasBL.get_cameraIp(bedId, "2")
    cameraIp3 = CamerasBL.get_cameraIp(bedId, "3")
    if (cameraIp1):
        requests.get("http://localhost:4000/connectRTSPCamera/1/" + cameraIp1)
    if (cameraIp2):
        requests.get("http://localhost:4000/connectRTSPCamera/2/" + cameraIp2)
    if (cameraIp3):
        requests.get("http://localhost:4000/connectRTSPCamera/3/" + cameraIp3)

    return jsonify(success=True)