import json
import src.Cameras.CamerasDal as CamerasDal


def get_cameraIp(bedId, typeId):
    return CamerasDal.get_cameraIp(bedId, camerasType[typeId])

def get_lastCameraIp(bedId, typeId):
    ipObject = CamerasDal.get_lastCameraIp(bedId, camerasType[typeId])
    if ipObject is not None:
        ipObject = ipObject["ip"]
    return ipObject

def add_cameraIP(bedId,typeId,cameraIP):
    if CamerasDal.get_cameraIp(bedId, camerasType[typeId]) is not None:
        CamerasDal.delete_camera(bedId, camerasType[typeId])
    CamerasDal.add_cameraIP(bedId,camerasType[typeId],cameraIP)

def delete_camera(bedId, typeId):
    CamerasDal.delete_camera(bedId, camerasType[typeId])

def update_cameras_file(update_json):
    with open(r'./Files\Cameras.json', 'w') as cameras_file:
        json.dump(update_json, cameras_file)


def get_measures(cameraTargetId):
    return cameraMeasures[cameraTargetId]


camerasType = {
    '1': "monitor",
    '2': "hanshama",
    '3': "mazrek"
}

cameraMeasures = {
    "1": "pulse,blood_pressure,average_blood_pressure,saturation,co2,temp",
    "2": "breath_rate,vte",
    "3": "injectorRate"
}