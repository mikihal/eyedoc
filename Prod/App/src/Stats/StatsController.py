import datetime
import json
from bson.json_util import dumps
from flask import Blueprint, jsonify, request
from flask_cors import cross_origin, CORS
import src.Cameras.CamerasBL as CamerasBL
import src.Patients.PatientsBL as PatientsBL
import src.Stats.StatsBL as statsBL
statsController = Blueprint('statsController', __name__)
lastAllStatsJson = []


@statsController.route('/allStats')
@cross_origin(origin="*")
def get_all_stats():
    allStatsJson = []
    auth = request.args.get('auth')
    patients = PatientsBL.get_allPatients()

    # ---------------could cause some problems---------------------
    if(auth != "1" and auth != "2"):
        return jsonify({"error": "Invalid Auth"})
    # ------------------------------------

    for patient in patients:
        currPatientDetails = {"id": patient["bedId"], "stats": {}}
        allPatientStats = statsBL.get_lastMonitorDataByBedId(patient["bedId"])
        for stats in allPatientStats:

            for stat in stats:
                currPatientDetails["stats"][stat] = {
                    "value": stats[stat], "level": 0}
                if stat == "blood_pressure":
                    currPatientDetails["stats"][stat]["level"] = statsBL.get_BPLevel(
                        stats[stat][0],  stats[stat][1], patient["severityData"])
                elif stat == "pulse":
                    currPatientDetails["stats"][stat]["level"] = statsBL.get_HRVLevel(
                        stats[stat], patient["severityData"])
                elif stat == "breath_rate":
                    currPatientDetails["stats"][stat]["level"] = statsBL.get_BRLevel(
                        stats[stat], int(patient["age"]), patient["severityData"])
                elif stat == "saturation":
                    currPatientDetails["stats"][stat]["level"] = statsBL.get_SATLevel(
                        stats[stat], patient["severityData"])
                elif stat == "temp":
                    currPatientDetails["stats"][stat]["level"] = statsBL.get_TEMPLevel(
                        stats[stat], patient["severityData"])
                elif stat == "co2":
                    currPatientDetails["stats"][stat]["level"] = statsBL.get_CO2Level(
                        stats[stat], patient["severityData"])

            if len(currPatientDetails["stats"]) == 0:
                currPatientDetails["stats"] = None
                currPatientDetails["severity"] = None
            elif "blood_pressure" in currPatientDetails["stats"] and "pulse" in currPatientDetails["stats"] and "breath_rate" in currPatientDetails["stats"]:
                # currPatientDetails["severity"] = statsBL.get_severityLevel(int(patient["age"]), currPatientDetails["stats"]["pulse"]["value"], currPatientDetails["stats"]
                #                                                            ["blood_pressure"]["value"], currPatientDetails["stats"]["breath_rate"]["value"], patient["severityData"])
                currPatientDetails["severity"] = statsBL.get_severityLevel(currPatientDetails["stats"],patient["severityData"],patient["age"]);

                global lastAllStatsJson
                lastCurrPatientDetails = next((x for x in lastAllStatsJson if x["id"] == currPatientDetails["id"]),
                                              None)
                if (lastCurrPatientDetails != None and "severity" in lastCurrPatientDetails.keys() and "level" in lastCurrPatientDetails["severity"].keys() and "lastUpdateTime" in lastCurrPatientDetails["severity"].keys()):
                    if (currPatientDetails["severity"]["level"] != lastCurrPatientDetails["severity"]["level"]):
                        currPatientDetails["severity"]["lastUpdateTime"] = datetime.datetime.now(
                        )
                    else:
                        currPatientDetails["severity"]["lastUpdateTime"] = lastCurrPatientDetails["severity"][
                            "lastUpdateTime"]
                else:
                    currPatientDetails["severity"]["lastUpdateTime"] = datetime.datetime.now(
                    )

        allStatsJson.append(currPatientDetails)

    lastAllStatsJson = allStatsJson
    return jsonify(allStatsJson)


@statsController.route('/stats/analysis/<bedId>/<cameraTargetId>/<measure>')
@cross_origin(origin="*")
def get_analysis_stats(bedId, cameraTargetId, measure):
    return jsonify(dumps(statsBL.get_measureData(bedId, cameraTargetId, measure)))


@statsController.route('/stats/analysis/<bedId>/<cameraTargetId>/<measure>/<minutes>')
@cross_origin(origin="*")
def get_analysis_stats_byTime(bedId, cameraTargetId, measure, minutes):
    return jsonify(dumps(statsBL.get_measureDataByTime(bedId, cameraTargetId, measure, minutes)))


@statsController.route('/image/<clientId>/<cameraTargetId>')
@cross_origin(origin="*")
def get_image(clientId, cameraTargetId):
    camera_ip = CamerasBL.get_cameraIp(clientId, cameraTargetId)
    data = jsonify([])
    if camera_ip:
        result = statsBL.get_lastMonitorData(clientId, camera_ip)
        if result != None:
            data = result["image"]
    return data

