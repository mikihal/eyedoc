from pymongo import MongoClient
import os
import json
import datetime
from bson.json_util import dumps
os.chdir(os.path.dirname(__file__))

dbClient = MongoClient("mongodb://localhost:27017/")
db = dbClient["eyedoc"]
Patients = db.get_collection("Patients")


#Patients
def get_allPatients():
    return Patients.find({"isDeleted": False})


#Patients
def add_newPatient(patient):
    return Patients.insert_one(patient)

def get_patientByBedId(bedId):
    return Patients.find_one({"bedId": bedId, "isDeleted": False})

#Patients
def editPatient(bedId, edittedPatient):
    return Patients.find_one_and_replace({"bedId": bedId, "isDeleted": False}, edittedPatient)

#Patients
def deletePatient(bedId):
    patientToDelete = Patients.find_one({"bedId": bedId , "isDeleted" : False})
    return Patients.update_one(patientToDelete, {"$set": {"isDeleted": True}})
