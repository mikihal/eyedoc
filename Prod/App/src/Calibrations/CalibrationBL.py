import src.Calibrations.CalibrationDal as CalibrationDal

def isCalibrationExists(cameraIp):
    isCalibExists = False
    if CalibrationDal.getLastCalibrationData(cameraIp) is not None:
        isCalibExists = True
    return isCalibExists