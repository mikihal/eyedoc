from pymongo import MongoClient

dbClient = MongoClient("mongodb://localhost:27017/")

db = dbClient["eyedoc"]

db.drop_collection("Patients")
db.drop_collection("Users")
db.drop_collection("Cameras")
db.drop_collection("MonitorData")

db.create_collection("Patients")
db.create_collection("Users")
db.create_collection("Cameras")
db.drop_collection("MonitorData")

users = db.get_collection("Users")
users.insert_many([{
    "id": "1",
    "name": "admin",
    "password": "12345",
    "admin": "true"
}, {
    "id": "2",
    "name": "laniado",
    "password": "laniado",
    "admin": "false"
}])

