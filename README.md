# EyeDoc.

Monitoring and control system that enables the medical staff
to watch real-time critical data from monitor, syringe pump and ventilator
outside of the contaminated area.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for deployment purposes.

### Prerequisites

1. Use python 3.6 [python](https://www.python.org/downloads/release/python-380/)

2. Use the package manager [pip](https://pip.pypa.io/en/stable/) to install packages for the project.

3. Use gitlab to clone the project files

4. Use IIS (Internet Information Services (IIS) Manager)
[start -> IIS]
If IIS doesnt exist on start menu, follow these instructions: [IIS](https://enterprise.arcgis.com/en/web-adaptor/latest/install/iis/enable-iis-8-components-server.htm)

5. Install MongoDB on your local machine.
Follow instructions from [MongoDB installation](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/)
Or from file "Prod/DB/installation.docx"

### Installing

1. Clone the project from [gitlab/eyedoc](https://gitlab.com/mikihal/eyedoc.git)

2. Locate files from "Prod/App/" direcory in your local "C:/inetpub/wwwroot/".

3. Install all pip packages that are listed in the "Prod/requirements.txt" file:

```
pip install <package-name>
```

4. Config database: 
make sure mongoDB Community is installed,
and that the mongoDB path is set to the enviroment variables PATH ("C:\Program Files\MongoDB\Server\4.2\bin\").
Create "C:/data/db" directory if it doesn't exist.
Run cmd as an administrator, then run to start up the server:
```
mongod
``` 
run cmd as an administrator and run "Prod/DB/db.py" file to build the db:
[you can set or change the app users and more default values from this file]   
```
python db.py
```
Check the server is up and responding.
*you can check this by running 'mongo' from cmd,
 and type 'use eyedoc' to see if db and collections exist. 

5. Open IIS -> right-click on the app name from the Sites list -> Edit Bindings -> change the port to 5000
then right-click on the app name again -> Manage Website -> start
browse for "localhost:5000" in chrome and check the app is running

6. Copy "camera-runner" from "Prod/" to your local "C:/" directory, 
then open cmd in the directory and run the command:

```
node index.js
``` 

7. Make sure the "vitalmonitor" service is located at your local "C:/" directory.
Open the main.py file and change the 
os.chdir(<...>) to:
```
os.chdir(r"C:\vitalmonitor")
```

8. Run vitalmonitor service, and try to add camera from the app.
if the python process is not running - check vitalmonitor process is running proper.

## Functionallity checks

If you followed all the instructions above,
the app should be fixed and ready for use.
Lets run some checks to verify that the basic functionallity is proper:

* Log in to the app with the administrative user you inserted into the db (see stage 4) 
* Add patient ((+) from the left of the search box)
* Add camera: ((+) from the top-left of one of the videos boxes).
  verify that the camera calibration window is opens and the camera is set up to the video box in the app.
* Switch toggle button to "מופחת" and verify that the camera you added is still alive. 
* Delete the patient by clicking the edit icon and (-). 

## Versioning

For the versions available, see the [Version](https://gitlab.com/mikihal/eyedoc/-/tags). 