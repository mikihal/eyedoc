//prevent the click on the screen to get to the App component , that will close the sidebar

const stopBubbling = (event) => {
    event.preventDefault();
    event.stopPropagation()
}

export default stopBubbling;