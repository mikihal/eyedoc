import React, { useState, useEffect } from 'react';

export default function Loader(props) {
    return (
        <svg className={`icon ${props.className}`} version="1.1" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
            <path d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#ffffff" stroke="none" transform="rotate(37.4769 50 51)">
                <animateTransform attributeName="transform" type="rotate" dur="0.8s" repeatCount="indefinite" keyTimes="0;1" values="0 50 51;360 50 51"></animateTransform>
            </path>
        </svg>
    )
}
