import React, { useState, useEffect } from 'react';
import './ParameterItem.css'

const fieldsDictionary = {
    "blood pressure": 'BP',
    "pulse": 'HRV',
    "breath rate": 'BR'
}
const nameToUnit = {
    "blood pressure": 'mmHg',
    "pulse": 'per min',
    "breath rate": 'per min'
}
export default function ParameterItem(props) {

    return (
        <div className={`parameter-item ${props.isFirst ? 'first' : props.isLast ? 'last' : ''}`}>
            <div className='value'>
                <span className='number'>{props.value}</span>
                <span className='unit'>{nameToUnit[props.name]}</span>
            </div>
            <div className='desc'>{fieldsDictionary[props.name]}</div>
        </div>
    )
}