import React, { useState, useEffect, useRef } from 'react';
import './alertDialog.css'
import stopBubbling from '../../Functions/StopBubbling';

const MESSAGE_TIMEOUT = 4000;
const AlertDialog = (props) => {
    const messageTimeout = useRef(false)

    const onClickOutside = (e) => {
        if (!props.onCancel)
            onApprove(e)
    }
    const onApprove = (e) => {
        if (messageTimeout.current) {
            clearTimeout(messageTimeout.current);
            messageTimeout.current = null;
        }
        if (e) {
            stopBubbling(e)
        }
        props.onApprove()
    }
    const onCancel = (e) => {
        stopBubbling(e)
        props.onCancel();
    }

    useEffect(() => {
        if (!props.onCancel) {
            messageTimeout.current = setTimeout(() => onApprove(), MESSAGE_TIMEOUT)
        }
        return () => {
            if (messageTimeout.current)
            {
                clearTimeout(messageTimeout.current);
                messageTimeout.current = null;
            }
        }
    }, [props.message])


    return (
        <div className='alert-box-container' onClick={onClickOutside}>
            <div className='alert-box'>
                <div className='alert-message'>{props.message}</div>
                <div className='buttons'>
                    <button className='approve' onClick={onApprove}>אישור</button>
                    {props.onCancel ? <button className='cancel' onClick={onCancel}>ביטול</button> : null}
                </div>
            </div>
        </div>
    )
}

export default AlertDialog