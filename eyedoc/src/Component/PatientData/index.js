import React, { useState, useEffect } from 'react';
import './PatientData.css'
import PatientDataItem from '../PatientDataItem';
import dataType from '../../Types/dataType';
import SeverityLevel from '../../Types/dataLevelType';
import PatientFinalStatusItem from '../PatientFinalStatusItem';
import reasons from '../../Types/reasons';
import { useAppModeContext } from '../../Pages/Home';
import applicationMode from '../../Types/applicationMode';
import CameraMode from '../../Types/cameraMode';
import AlertDialog from '../AlertDialog/AlertDialog';
import { useSelectedStatsContext } from '../../Pages/StreamsView';

const SLIM_MODE_MESSAGE = 'המערכת במצב מופחת'
const TITLE = 'נתונים מרכזיים'

const cameraModeIcons = {
    [CameraMode.FULL]: 'fas fa-compress',
    [CameraMode.PARTIAL]: 'fas fa-expand'
}

export default function PatientData(props) {
    const { appMode } = (useAppModeContext());
    const { selectedStats, updateSelectedStats } = useSelectedStatsContext();

    const onStatSelect = (statKey) => {
        updateSelectedStats(statKey)
    }

    const sortSelectedStats = (a, b) => {
        const aIndex = Object.keys(dataType).indexOf(a)
        const bIndex = Object.keys(dataType).indexOf(b)

        if (aIndex < bIndex) {
            return -1;
        }
        if (aIndex > bIndex) {
            return 1;
        }
        return 0;
    }

    const patientData = ((props.cameraMode === CameraMode.PARTIAL) ? selectedStats.sort(sortSelectedStats) :
        Object.keys(dataType)).map((dataTypeKey) => {
            return (
                <PatientDataItem
                    key={Object.keys(dataType).indexOf(dataTypeKey)}
                    itemName={dataType[dataTypeKey]}
                    itemData={(props.patientData && props.patientData.stats) ? props.patientData.stats[dataType[dataTypeKey]] : null}
                    isSelected={selectedStats.includes(dataTypeKey)}
                    onClick={() => onStatSelect(dataTypeKey)}
                    selectionAvailable={props.cameraMode === CameraMode.FULL}
                />
            )
        })


    const itemsToRender = (appMode === applicationMode.REGULAR) ?
        [
            <div className='patient-data-title'>
                <div className='icons'>
                    <div className={`icon ${(cameraModeIcons[props.cameraMode])}`} onClick={() => props.changeSelectedCamera(props.videoName)}></div>
                </div>
                <div className='name'>{TITLE}</div>
                <div className='icons'>

                </div>
                <div></div>
            </div>,
            <div className='items-holder'>
                {patientData}
            </div>
        ] :
        [
            <span className='slim-mode-message'>{SLIM_MODE_MESSAGE}</span>
        ]

    return (
        <div className={`patient-data stream-view-item ${props.className} ${props.cameraMode}`}>
            <div className='holder'>
                {itemsToRender}
            </div>
        </div>
    )
}