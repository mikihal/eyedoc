
import React from 'react'
import './tooltip.css'
import reasons from '../../Types/reasons'

export default function Tooltip(props) {
    /**
     * From props
     * - isVisable
     * - coords
     * - text
     * --- need to check if I cause to much updates because of the 3 listeners on each div
     */

    const tooltipItem = (props.isVisable && props.userCoords) ?
        <span className='tooltip-item' style={{ left: props.userCoords.x , top: props.userCoords.y - (0.05 * window.innerHeight) }}>
            {(props.tooltipMessage)}
        </span> : null

    return (
        <div className='tooltip'>
            {tooltipItem}
        </div>
    )
}