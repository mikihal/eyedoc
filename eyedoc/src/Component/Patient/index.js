import React, { useState, useEffect } from 'react';
import axios from 'axios'
import SeverityLevel from '../../Types/dataLevelType';
import './patient.css'
import BloodPressure from '../../Assets/Svgs/BloodPressure';
import HeartBeat from '../../Assets/Svgs/HeartBeat';
import dataType from '../../Types/dataType';
import AlertDialog from '../AlertDialog/AlertDialog'
import ServerIp from '../../Config'
import Thresholds from '../Thresholds/Thresholds';
import reasons from '../../Types/reasons';
import { getAuth } from '../../Auth/AuthApi';
import stopBubbling from '../../Functions/StopBubbling';
import Question from '../../Assets/Svgs/Question';
import { useAppModeContext } from '../../Pages/Home';
import applicationMode from '../../Types/applicationMode';
import SeverityTime from '../SeverityTime';
import severityDataType from '../../Types/severityDataType';
import BreathRate from '../../Assets/Svgs/BreathRate';

export { ServerIp, dataType }

const NO_STATS = '---'
const SEVERITY_UPDATE_TIMEOUT = 5000; // 5s

const dictionary = {
    bed: 'מיטה',
    patient: 'מטופל'
}
const iconsNames = {
    [dataType.BLOOD_PRESSURE]: 'BP',
    [dataType.PULSE]: 'HRV',
    [dataType.BREATH_RATE]: 'BR',
}
const severityDictionary = {
    [SeverityLevel.CRITICAL]: 'קריטי',
    [SeverityLevel.HIGH]: 'קשה',
    [SeverityLevel.MEDIUM]: 'בינוני',
    [SeverityLevel.NORMAL]: 'יציב',
}
const Patient = (props) => {

    const [isEditMode, setIsEditMode] = useState(false)
    const [id, setId] = useState(props.patient.id)
    const [age, setAge] = useState(props.patient.age)
    const [firstname, setFirstname] = useState(props.patient.firstName)
    const [lastname, setLastname] = useState(props.patient.lastName)
    const [alertMessage, setAlertMessage] = useState(null)
    const [isThresholdsEditOpen, setIsThresholdsEditOpen] = useState(false)
    const [updatedThresholds, setUpdatedThresholds] = useState({})
    const [currntTime, setCurrntTime] = useState(Date.now())
    // the current selected severity
    const [currentSeverityLevel, setCurrentSeverityLevel] = useState(null)
    // the reason for the currrent severity
    const [currentSeverityReason, setCurrentSeverityReason] = useState(null)
    // the time the current severity was updated 
    const [severityUpdateTime, setSeverityUpdateTime] = useState(null)
    // the time the server send a different severity, it need to keep that severity for SEVERITY_UPDATE_TIMEOUT to make it the current severity
    const [updateSeverityRequestTime, setUpdateSeverityRequestTime] = useState(null)

    const [ageError, setAgeError] = useState('')
    const [idError, setIdError] = useState('')
    const [validAge, setValidAge] = useState(true);
    const [validId, setValidId] = useState(true);


    const userID = getAuth()
    const { appMode } = useAppModeContext()

    const tick = () => {
        setCurrntTime(Date.now())
    }

    useEffect(() => {
        const interval = setInterval(tick, 1000)
        return () => clearInterval(interval);
    }, [])

    useEffect(() => {
        setIsThresholdsEditOpen(false)
    }, [isEditMode])

    useEffect(() => {
        setFirstname(props.patient.firstName)
        setLastname(props.patient.lastName)
        setAge(props.patient.age)
        setId(props.patient.id)
    }, [props.patient])

    useEffect(() => {

        if ((props.patientData && props.patientData.severity && props.patientData.severity.level && props.patientData.severity.lastUpdateTime)) {
            let requestTime = new Date(props.patientData.severity.lastUpdateTime)
            requestTime.setHours(requestTime.getHours() - 3);

            if (!currentSeverityLevel) {
                setCurrentSeverityLevel(props.patientData.severity.level)
                setCurrentSeverityReason(props.patientData.severity.reason)
                setUpdateSeverityRequestTime(requestTime.getTime())
                setSeverityUpdateTime(requestTime.getTime())

                props.updatePatientSeverity(props.patient.bedId, props.patientData.severity.level, requestTime.getTime())
            }
            else {
                if (currentSeverityLevel === props.patientData.severity.level) {
                    // can change reasons on the same status without waiting 
                    if (currentSeverityReason !== props.patientData.severity.reason) {
                        setCurrentSeverityReason(props.patientData.severity.reason)
                    }
                }
                else if (updateSeverityRequestTime !== requestTime.getTime()) {
                    setUpdateSeverityRequestTime(requestTime.getTime())
                }
                // the patient kept his status for SEVERITY_UPDATE_TIMEOUT seconds, its time for an update!
                else if (Math.ceil(currntTime - updateSeverityRequestTime) > SEVERITY_UPDATE_TIMEOUT) {
                    setCurrentSeverityLevel(props.patientData.severity.level)
                    setCurrentSeverityReason(props.patientData.severity.reason)
                    setSeverityUpdateTime(updateSeverityRequestTime);

                    props.updatePatientSeverity(props.patient.bedId, props.patientData.severity.level, updateSeverityRequestTime)
                }
            }
        }
        else {
            props.removeFromPatientSevirityList(props.patient.bedId)
            setCurrentSeverityLevel(null)
            setCurrentSeverityReason(reasons[6])
            setUpdateSeverityRequestTime(null)
            setSeverityUpdateTime(null)
        }
    }, [props.patientData])

    const discardChanges = () => {
        props.setPatientUnsavedChanges(false)
        props.setUnsavedChangesAlertVisibility(false)
        setIsEditMode(false)
    }

    const editPatient = (severityData) => {

        axios.post(`http://${ServerIp}/editPatient`,
            {
                bedId: props.patient.bedId,
                firstName: firstname,
                lastName: lastname,
                age: age,
                id: id,
                severityData: !isThresholdsEditOpen ? severityData : updatedThresholds,
                userId: userID
            }).then(response => {
                props.getAllPatients();
                setAlertMessage(<AlertDialog onApprove={() => setAlertMessage(null)} message={response.data.message}></AlertDialog>)
            })
        props.setPatientUnsavedChanges(false)
        setIsEditMode(false)
    }

    const deletePatient = () => {

        axios.post('http://' + ServerIp + '/deletePatient',
            {
                identifier: props.patient.bedId,
                firstName: props.patient.firstName,
                lastName: props.patient.lastName,
                userId: userID
            }).then(response => {
                setAlertMessage(<AlertDialog onApprove={() => { setAlertMessage(null); props.getAllPatients(); }} message={response.data.message}></AlertDialog>)
            })
        setIsEditMode(false)
    }

    const checkAuthorityToDelete = () => {
        const authority = getAuth()
        authority ? setAlertMessage(<AlertDialog onCancel={() => setAlertMessage(null)} onApprove={() => { deletePatient(); }} message={"האם ברצונך למחוק את המטופל מהרשימה?"}></AlertDialog>)
            : setAlertMessage(<AlertDialog onApprove={() => { setAlertMessage(null); }} message={"אין לך הרשאה מתאימה למחיקת מטופל"}></AlertDialog>)
    }

    const onMouseEnter = (e, selectedItem) => {
        stopBubbling(e)
        props.enableTooltip({
            x: e.clientX,
            y: e.clientY
        },
            getTooltipMessage(selectedItem)
        );
    }
    const getTooltipMessage = (selectedItem) => {
        let tooltipMessage = null
        switch (selectedItem) {
            case 'severity':
                tooltipMessage = (currentSeverityLevel && currentSeverityReason != null)
                    ? getSeverityMessage(currentSeverityReason)
                    :
                    reasons[Object.keys(reasons).length - 1]
                break;
            case 'severityTime':
                const hoursFromLastUpdate = Math.ceil((currntTime - severityUpdateTime) / 36e5);
                tooltipMessage = `המטופל במצב ${severityDictionary[currentSeverityLevel]} במשך כ ${hoursFromLastUpdate} ש'`
                break;
            default:
                tooltipMessage = iconsNames[selectedItem]
                break;
        }

        return tooltipMessage
    }
    const getSeverityMessage = (currentSeverityReason) => {
        let message = ""
        let isHRVBigger;
        try {
            switch (currentSeverityReason) {
                case 0:
                    message = reasons[0]
                    break;
                case 1:
                    message = `לחץ דם סיסטולי נמוך מערך הסף שהוצב למטופל (${props.patient.severityData.minSBP}) 
                     סטורציה נמוכה מערך הסף שהוצב למטופל (${props.patient.severityData.minSAT})`
                    break;
                case 2:
                    isHRVBigger = (props.patientData.stats[dataType.PULSE].value > props.patient.severityData.maxHRV)
                    message = `סטורציה נמוכה מערך הסף שהוצב למטופל (${props.patient.severityData.minSAT}) 
                     קצב נשימתי גבוה מהערך הסף שהוצב למטופל (${props.patient.severityData.mediumBR}) 
                      דופק ${isHRVBigger ? 'גבוה' : 'נמוך'} מערך הסף שהוצב למטופל (${props.patient.severityData[isHRVBigger ? 'maxHRV' : 'minHRV']}) `
                    break;
                case 3:
                    isHRVBigger = (props.patientData.stats[dataType.PULSE].value > props.patient.severityData.maxHRV)
                    message = `לחץ דם סיסטולי נמוך מערך הסף שהוצב למטופל (${props.patient.severityData.minSBP}) 
                     דופק ${isHRVBigger ? 'גבוה' : 'נמוך'} מערך הסף שהוצב( ${props.patient.severityData[isHRVBigger ? 'maxHRV' : 'minHRV']})`
                    break;
                case 4:
                    message = `לחץ דם סיסטולי נמוך מערך הסף שהוצב למטופל (${props.patient.severityData.minSBP}) 
                     קצב נשימתי גבוה מערך הסף שהוצב למטופל (${props.patient.severityData.mediumBR})`
                    break;
                case 5:
                    message = `לחץ דם סיסטולי נמוך מערך הסף שהוצב למטופל (${props.patient.severityData.minSBP})`
                    break;
                case 6:
                    message = `סטורציה נמוכה מערך הסף שהוצב למטופל (${props.patient.severityData.minSAT})`
                    break;
                case 7:
                    message = `קצב נשימתי גבוה מערך הסף שהוצב למטופל (${props.patient.severityData.mediumBR})`
                    break;
                case 8:
                    isHRVBigger = (props.patientData.stats[dataType.PULSE].value > props.patient.severityData.maxHRV)
                    message = `דופק ${isHRVBigger ? 'גבוה' : 'נמוך'} מערך הסף שהוצב (${props.patient.severityData[isHRVBigger ? 'maxHRV' : 'minHRV']})`
                    break;
                case 9:
                    message = reasons[9]
                    break;
                default:
                    break;
            }
        } catch (e) {
            message = ""
        }
        return message
    }
    const onMouseMove = (e) => {
        stopBubbling(e)
        props.updateTooltipCoords({
            x: e.clientX,
            y: e.clientY
        })
    }
    const onMouseLeave = (e) => {
        stopBubbling(e)
        props.disableTooltip();
    }

    const validateId = (e) => {
        let validation = /^[0-9]{8,9}$/;
        if (e.match(validation)) {
            setIdError('');
            setValidId(true);
            setId(e)
        }
        else {
            setIdError('נא להכניס מספר בן 9-8 ספרות');
            setValidId(false);
        }
    }

    const validateAge = (e) => {
        let validation = /^(?:1[01][0-9]|120|[1-9]|[1-9][0-9])$/;
        if (e.match(validation)) {
            setAgeError('')
            setValidAge(true)
            setAge(e)
        }
        else {
            setAgeError('נא להכניס גיל בין 1 ל 120');
            setValidAge(false)
        }
    }

    const valuesToRender = (appMode == applicationMode.REGULAR) ?
        [
            <div className='value'>
                <span>
                    {
                        (props.patientData && props.patientData.stats && props.patientData.stats[dataType.BLOOD_PRESSURE]) ?
                            ` ${props.patientData.stats[dataType.BLOOD_PRESSURE].value[0]}/${props.patientData.stats[dataType.BLOOD_PRESSURE].value[1]}`
                            :
                            NO_STATS
                    }
                </span>
                <BloodPressure
                    onMouseEnter={(e) => onMouseEnter(e, dataType.BLOOD_PRESSURE)}
                    onMouseLeave={(e) => onMouseLeave(e)}
                    onMouseMove={(e) => onMouseMove(e)}
                    className={(props.patientData && props.patientData.stats && props.patientData.stats[dataType.BLOOD_PRESSURE]) ? props.patientData.stats[dataType.BLOOD_PRESSURE].level : SeverityLevel.NORMAL} />
            </div>,
            <div className='value'>
                <span>
                    {
                        (props.patientData && props.patientData.stats && props.patientData.stats[dataType.PULSE]) ? props.patientData.stats[dataType.PULSE].value : NO_STATS
                    }
                </span>
                <HeartBeat
                    onMouseEnter={(e) => onMouseEnter(e, dataType.PULSE)}
                    onMouseLeave={(e) => onMouseLeave(e)}
                    onMouseMove={(e) => onMouseMove(e)}
                    className={
                        (props.patientData && props.patientData.stats && props.patientData.stats[dataType.PULSE]) ? props.patientData.stats[dataType.PULSE].level : SeverityLevel.NORMAL
                    } />
            </div>,
            <div className='value'>
                <span>
                    {
                        (props.patientData && props.patientData.stats && props.patientData.stats[dataType.BREATH_RATE]) ? props.patientData.stats[dataType.BREATH_RATE].value : NO_STATS
                    }
                </span>
                <BreathRate
                    onMouseEnter={(e) => onMouseEnter(e, dataType.BREATH_RATE)}
                    onMouseLeave={(e) => onMouseLeave(e)}
                    onMouseMove={(e) => onMouseMove(e)}
                    className={(props.patientData && props.patientData.stats && props.patientData.stats[dataType.BREATH_RATE]) ? props.patientData.stats[dataType.BREATH_RATE].level : SeverityLevel.NORMAL} />
            </div>,
            <div className='value info-icon'>
                <Question
                    className={
                        `icon severity-explanation ${(currentSeverityLevel) ? currentSeverityLevel : SeverityLevel.NORMAL}`
                    }
                    onMouseEnter={(e) => onMouseEnter(e, 'severity')}
                    onMouseLeave={(e) => onMouseLeave(e)}
                    onMouseMove={(e) => onMouseMove(e)}
                />
            </div>,
        ] : null


    const onPatientClick = () => {
        if (props.isDisabled) {
            props.setUnsavedChangesAlertVisibility(true)
        } else {
            props.onClick()
        }
    }
    const setEditModeVisibility = () => {
        if (props.isDisabled) {
            props.setUnsavedChangesAlertVisibility(true)
        } else {
            props.onClick()
            setIsEditMode(!isEditMode)
            if (isEditMode)
                props.setPatientUnsavedChanges(false)
        }
    }

    return (
        <div
            className={`patient ${props.className} ${isEditMode ? (isThresholdsEditOpen ? 'edit-mode edit-thresholds-mode' : 'edit-mode') : ''} border-${(appMode == applicationMode.REGULAR) ? currentSeverityLevel : SeverityLevel.NORMAL}`}
            onClick={onPatientClick}>
            {alertMessage}
            <div className={`screen-locker ${(props.patientUnsavedChanges) && !(props.isDisabled) ? 'enabled' : 'disabled'}`} onClick={() => props.setUnsavedChangesAlertVisibility(true)} />
            <div className="icon-holder">
                {(userID == 1) ?
                    <img
                        className='edit'
                        onClick={setEditModeVisibility}
                        alt=''
                        src={isEditMode ? require('../../Assets/Icons/cancel.svg') : require('../../Assets/Icons/edit.png')}
                    /> : null}
                <div className='title'>
                    {`${dictionary.bed} ${props.patient.bedId} - ${props.patient.firstName} ${props.patient.lastName}`}
                </div>
                {isEditMode ? <img className='delete' alt="" onClick={checkAuthorityToDelete} src={require('../../Assets/Icons/trash.png')}></img> : null}
            </div>
            {
                isEditMode ? <div className='edit-section'>
                    <div className='field'>
                        <span className='name'>ת.ז:</span>
                        <input className='box' type='number' defaultValue={props.patient.id} onChange={(e) => { props.setPatientUnsavedChanges(true); validateId(e.target.value) }}></input>
                    </div>
                    <span className='validation'>{idError}</span>
                    <div className='field'>
                        <span className='name'>שם פרטי:</span>
                        <input className='box' type='text' onChange={(e) => { props.setPatientUnsavedChanges(true); setFirstname(e.target.value) }} defaultValue={props.patient.firstName} ></input>
                    </div>
                    <div className='field'>
                        <span className='name'>שם משפחה:</span>
                        <input className='box' type='text' defaultValue={props.patient.lastName} onChange={(e) => { props.setPatientUnsavedChanges(true); setLastname(e.target.value) }}></input>
                    </div>
                    <div className='field'>
                        <span className='name'>גיל:</span>
                        <input className='box' type='number' min='1' max='120' defaultValue={props.patient.age} onChange={(e) => { props.setPatientUnsavedChanges(true); validateAge(e.target.value) }}></input>
                    </div>
                    <span className='validation'>{ageError}</span>

                    <Thresholds discardChanges={discardChanges}
                        isDisabled={props.isDisabled}
                        onUpdate={() => props.setPatientUnsavedChanges(true)}
                        validateAge={validAge} validateId={validId}
                        setIsThresholdOpen={setIsThresholdsEditOpen}
                        setUpdatedThresholds={setUpdatedThresholds}
                        isOpen={isThresholdsEditOpen}
                        editPatient={editPatient}
                        severityData={props.patient.severityData}
                        showUnsavedChangesAlert={props.showUnsavedChangesAlert}
                        setUnsavedChangesAlertVisibility={props.setUnsavedChangesAlertVisibility}

                    />
                </div> : null
            }
            <div className='values'>
                {valuesToRender}
            </div>
            <div className='last-update'>
                {(appMode === applicationMode.REGULAR && currentSeverityLevel && severityUpdateTime) ?
                    <SeverityTime severityLevel={currentSeverityLevel} currentTime={currntTime} severityUpdateTime={severityUpdateTime}
                        onMouseEnter={(e) => onMouseEnter(e, 'severityTime')}
                        onMouseLeave={(e) => onMouseLeave(e)}
                        onMouseMove={(e) => onMouseMove(e)}
                    />
                    :
                    null
                }
            </div>
        </div >
    )
}

export default Patient