import React, { useState, useEffect } from 'react'
import './UserHistory.css'
import { useAppModeContext } from '../../Pages/Home';
import DisplayMode from '../../Types/DisplayMode';
import ApplicationMode from '../../Types/applicationMode';
import { getAuth } from '../../Auth/AuthApi';
import Permission from '../../Types/Permission';
import Axios from 'axios';
import { ServerIp } from '../Patient';
import CameraMode from '../../Types/cameraMode';
import Analysis from '../Analysis/Analysis';
import IPDialog from '../IPDialog';


const dictionary = {
    dumbbellMachine: 'מכונת הנשמה',
    monitor: 'מוניטור',
    injectors: 'משאבת מזרק'
}
const cameraModeIcons = {
    [CameraMode.FULL]: 'fas fa-compress',
    [CameraMode.PARTIAL]: 'fas fa-expand'
}
const startCalibration = (clientId, cameraIp, cameraTargetId) => {
    Axios.get("http://" + ServerIp + "/connectCamera/" + clientId + "/" + cameraIp + "/" + cameraTargetId)
}

const deleteCamera = (bedId, cameraIndex) => {
    Axios.get("http://" + ServerIp + "/deleteCamera/" + bedId + "/" + cameraIndex)
}

export default function UserHistory(props) {

    const [ipDialogOpened, setIPDialogOpen] = useState(false);
    const [isSpinnerVisable, setSpinnerVisability] = useState(false)
    const { appMode } = useAppModeContext()


    const getFunctionIcon = () => {
        let icon = null
        icon = icon = <div className='icon'></div>
        if (getAuth() == Permission.ADMIN) {
            icon = <div className='icon fas fa-plus'
                onClick={() => {
                    setIPDialogOpen(true);
                    deleteCamera(props.bedId, props.cameraIndex)
                }} />
        } else {
            icon = null
        }

        return icon;
    }

    return (
        <div className={`user-history ${props.cameraMode}`}>
            <div className='user-history-title'>
                <div className='icons multi'>
                    <div className='icon fas fa-video' onClick={props.changeDisplayMode} />
                </div>
                <div className='name'> {dictionary[props.videoName]}</div>
                <div className='icons one'>
                    <div className={`icon ${(cameraModeIcons[props.cameraMode])}`} onClick={() => props.changeSelectedCamera(props.videoName)}></div>
                </div>
            </div>
            <div className='graph-place'>
                {
                    (props.isVideoExist) ?
                        <Analysis
                            cameraIndex={props.cameraIndex}
                            severityData={props.severityData}
                            bedId={props.bedId}
                            displayMode={props.displayMode}
                        />
                        :
                        <IPDialog />
                }
            </div>
        </div>
    )
}
