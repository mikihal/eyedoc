import React, { } from 'react';
import './PatientDataItem.css'
import dataType from '../../Types/dataType';
import SeverityLevel from '../../Types/dataLevelType'
import AverageBloodPressure from '../../Assets/Svgs/AverageBloodPressure';
import Saturation from '../../Assets/Svgs/Saturation';
import BloodPressure from '../../Assets/Svgs/BloodPressure';
import HeartBeat from '../../Assets/Svgs/HeartBeat';
import BreathRate from '../../Assets/Svgs/BreathRate';
import Vaccine from '../../Assets/Svgs/Vaccine';
import SkinTemp from '../../Assets/Svgs/SkinTemp';
import Measurement from '../../Assets/Svgs/Measurement';
import CO2 from '../../Assets/Svgs/CO2.js';


const nameToUnit = {
    [dataType.BLOOD_PRESSURE]: 'mmHg',
    [dataType.PULSE]: 'per min',
    [dataType.BREATH_RATE]: 'per min',
    [dataType.AVERAGE_PRESSURE]: 'mmHg',
    [dataType.SATORATION]: '%',
    [dataType.INJECTOR_RATE]: 'mml per hour',
    [dataType.TEMP]: '°C',
    [dataType.VTE]: 'cm³',
    [dataType.CO2]: 'mmHg'
}

const nameDictionary = {
    [dataType.BLOOD_PRESSURE]: 'BP',
    [dataType.PULSE]: 'HRV',
    [dataType.BREATH_RATE]: 'BR',
    [dataType.SATORATION]: 'SAT',
    [dataType.AVERAGE_PRESSURE]: 'MAP',
    [dataType.INJECTOR_RATE]: 'IR',
    [dataType.TEMP]: 'TEMP',
    [dataType.VTE]: 'VTE',
    [dataType.CO2]: 'CO2',
}


export default function PatientDataItem(props) {

    const getIcon = () => {
        const iconClassName = `icon ${(props.itemData && props.itemData.level) ? props.itemData.level : SeverityLevel.NORMAL}`
        let icon = null

        switch (props.itemName) {
            case dataType.AVERAGE_PRESSURE:
                icon = <AverageBloodPressure className={iconClassName} />
                break;
            case dataType.BLOOD_PRESSURE:
                icon = <BloodPressure className={iconClassName} />
                break;
            case dataType.SATORATION:
                icon = <Saturation className={iconClassName} />
                break;
            case dataType.BREATH_RATE:
                icon = <BreathRate className={iconClassName} />
                break;
            case dataType.PULSE:
                icon = <HeartBeat className={iconClassName} />
                break;
            case dataType.INJECTOR_RATE:
                icon = <Vaccine className={iconClassName} />
                break;
            case dataType.TEMP:
                icon = <SkinTemp className={iconClassName} />
                break;
            case dataType.CO2:
                icon = <CO2 className={iconClassName} />
                break;
            case dataType.VTE:
                icon = <Measurement className={iconClassName} />
                break;
            default:
                icon = null
                break;
        }
        return icon;
    }


    const getValue = () => {
        let value = '---'
        if (props.itemData && props.itemData.value != "") {
            switch (props.itemName) {
                case dataType.BLOOD_PRESSURE:
                    value = `${props.itemData.value[0]}/${props.itemData.value[1]}`
                    break;
                default:
                    value = props.itemData.value
            }
        }

        return value;
    }

    const getUnit = () => {
        let unit = ""
        if (props.itemData && props.itemData.value) {
            unit = nameToUnit[props.itemName]
        }

        return unit;
    }

    return (
        <div className={`patient-data-item ${props.itemName}`} onClick={(props.selectionAvailable ? props.onClick : null)}>
            {(props.selectionAvailable) ? <div className={`icon selection ${(props.isSelected) ? 'far fa-check-circle' : ''}`}></div> : null}
            <div className='desc-box'>
                <div className='name'>{nameDictionary[props.itemName]}</div>
                <div className='icon-box'>
                    {getIcon()}
                </div>
            </div>
            <div className='value-box'>
                <span className='value' >{getValue()}</span>
                <span className='unit'>{getUnit()}</span>
            </div>
        </div>
    )
}