import React, { useState, useEffect } from 'react'
import './thresholds.css'
import AlertDialog from '../AlertDialog/AlertDialog'
import THRESHOLDS_KEYS from '../../Types/thresholds.js'
import SeverityDataType from '../../Types/severityDataType.js'

const dictionary = {
    HRV_ERROR: "נא להכניס ערך בין 0 ל 350",
    HRV_VALIDATION: (value) => value >= 0 && value <= 350,
    BP_ERROR: "נא להכניס ערך בין 0 ל 300",
    BP_VALIDATION: (value) => value >= 0 && value < 300,
    MAP_ERROR: "נא להכניס ערך בין 0 ל 300",
    MAP_VALIDATION: (value) => value >= 0 && value <= 300,
    BR_ERROR: "נא להכניס ערך בין 0 ל 99",
    BR_VALIDATION: (value) => value >= 0 && value < 100,
    SAT_ERROR: "נא להכניס ערך בין 0 ל 100",
    SAT_VALIDATION: (value) => value >= 0 && value <= 100,
    CO2_ERROR: "נא להכניס ערך בין 0 ל 99",
    CO2_VALIDATION: (value) => value >= 0 && value < 100,
    TEMP_ERROR: "נא להכניס ערך בין 25 ל 45",
    TEMP_VALIDATION: (value) => value >= 25 && value <= 45,
}

let invalidField = ''

const Thresholds = (props) => {

    const updatedThresholds = props.severityData
    const [alertMessage, setAlertMessage] = useState(null)

    const setThresholdValue = (key, value) => {
        invalidField = ''
        Object.assign(updatedThresholds, { [key]: parseInt(value) })
        props.onUpdate()
    }

    const applyChanges = () => {
        if (invalidField == '') {
            let updatedSeverity = props.severityData
            Object.assign(updatedSeverity, updatedThresholds)
            props.editPatient(updatedSeverity)
        } else {
            setAlertMessage(<AlertDialog onApprove={() => setAlertMessage(null)} message='יש למלא את כל השדות'></AlertDialog>)
        }
    }

    const NAME_INDEX = 0
    const VALUE_INDEX = 1

    const validate = (name, value) => {
        let messageError = null
        let validation = null
        switch (name) {
            case "minSBP":
            case "maxSBP":
            case "mediumSBP":
                messageError = dictionary.BP_ERROR
                validation = dictionary.BP_VALIDATION
                break;
            case "minDBP":
            case "maxDBP":
            case "mediumDBP":
                messageError = dictionary.BP_ERROR
                validation = dictionary.BP_VALIDATION
                break;
            case "minMAP":
            case "maxMAP":
                messageError = dictionary.MAP_ERROR
                validation = dictionary.MAP_VALIDATION
                break;
            case "minHRV":
            case "maxHRV":
                messageError = dictionary.HRV_ERROR
                validation = dictionary.HRV_VALIDATION
                break;
            case "minCO2":
                messageError = dictionary.CO2_ERROR
                validation = dictionary.CO2_VALIDATION
                break;
            case "maxTEMP":
            case "minTEMP":
            case "mediumTEMP":
                messageError = dictionary.TEMP_ERROR
                validation = dictionary.TEMP_VALIDATION
                break;
            case "minSAT":
                messageError = dictionary.SAT_ERROR
                validation = dictionary.SAT_VALIDATION
                break;
            case "mediumBR":
            case "maxBR":
                messageError = dictionary.BR_ERROR
                validation = dictionary.BR_VALIDATION
                break;
            default:
                break
        }
        if (validation != null && !validation(value)) {
            invalidField = name
            return messageError
        } else {
            if (invalidField == name) {
                invalidField = ''
            }
        }
        return null
    }

    const thresholdsFields = Object.entries(props.severityData).map((threshold, index) => {
        return (
            <div>
                <div key={index} className='field'>
                    <span className='name'>{THRESHOLDS_KEYS[threshold[NAME_INDEX]]}:</span>
                    <input className='box' defaultValue={threshold[VALUE_INDEX]} type='number' min='0' onChange={(e) => setThresholdValue(threshold[NAME_INDEX], e.target.value)}></input>
                </div>
                <span className='validation'>{validate(threshold[NAME_INDEX], threshold[VALUE_INDEX])}</span>
            </div>
        );
    })

    const closeThresholds = () => {
        let updatedSeverity = props.severityData
        Object.assign(updatedSeverity, updatedThresholds)
        props.setUpdatedThresholds(updatedThresholds)
        props.setIsThresholdOpen(!props.isOpen)
    }

    useEffect(() => {
        if (props.showUnsavedChangesAlert && !props.isDisabled) {
            setAlertMessage(<AlertDialog onApprove={saveAll} onCancel={discardChanges} message={"האם ברצונך לשמור את השינויים?"}></AlertDialog>)
        }
    }, [props.showUnsavedChangesAlert])

    const saveAll = () => {
        // here i need to save the data of the patient
        setAlertMessage(null)
        props.setUnsavedChangesAlertVisibility(false);
        applyChanges()
    }
    const discardChanges = () => {
        setAlertMessage(null)
        props.discardChanges()
    }

    return (
        <div className='thresholds'>
            {alertMessage}
            {!props.isOpen ? <div className='threshold-text' onClick={closeThresholds}>
                <span>ערכי סף</span>
                <img className='threshold-icon'
                    onClick={closeThresholds}
                    src={require('../../Assets/Icons/more.png')} />
            </div> :
                <img className='threshold-icon'
                    onClick={closeThresholds}
                    src={require('../../Assets/Icons/less.png')} />}
            {props.isOpen ?
                <div className='title'>
                    <span>ערכי סף:</span>
                </div> : null}
            {props.isOpen ?
                <div className='fields'>
                    {thresholdsFields}
                </div>
                : null}
            <div className='apply'>
                <span>שמור</span>
                <img onClick={applyChanges} src={require('../../Assets/Icons/check.svg')}></img>
            </div>
        </div>
    );
}
export default Thresholds