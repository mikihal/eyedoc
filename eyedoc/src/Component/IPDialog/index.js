import React, { useState, useEffect } from 'react';
import './IPDialog.css'
import { getAuth } from '../../Auth/AuthApi';
import permission from '../../Types/Permission';
import Loader from '../../Assets/Svgs/Loader';
import applicationMode from '../../Types/applicationMode';
import Axios from 'axios';
import ServerIp from '../../Config'
import { useAppModeContext } from '../../Pages/Home';

const Dictionary = {
    calibrate: 'כייל מצלמה',
    add: 'הוסף מצלמה',
    savedCalib: 'השתמש בכיול קיים'
}
const getTitleByPermission = {
    [permission.ADMIN]: 'לא הוזנה מצלמה, אנא הזן כתובת IP של המצלמה המבוקשת',
    [permission.GUEST]: 'לא הוזנה מצלמה, אנא פנה לאדמין לסידור בעיה זאת',
}
export default function IPDialog(props) {

    const [inputValue, setInputValue] = useState("")
    const [isInTimeout, setIsInTimeout] = useState(false)
    const [isCalibSaved, setIsCalibSaved] = useState(false)
    const [isInputValid, setIsInputValid] = useState(false)
    const { appMode } = useAppModeContext();

    const isInputValidCheck = (inputValue) => {
        const regex = /^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$/;
        if (regex.test(inputValue)) {
            setIsInputValid(true)
            isCalibSavedCheck(inputValue)
        } else {
            setIsInputValid(false)
            setIsCalibSaved(false)
        }
    }

    const isCalibSavedCheck = (inputValue) => {
        Axios.get('http://' + ServerIp + '/isCalibrationSaved/' + inputValue).then((res) => {
            setIsCalibSaved(res.data)
        })
    }

    const onClick = (isExists) => {
        props.onSubmit(props.bedId, inputValue, props.cameraIndex, isExists);
        setIsInTimeout(true)
        setTimeout(() => setIsInTimeout(false), 10000)
    }


    const getItem = () => {
        let item = null
        if (!props.dialogOpened) {
            item = null;
        } else if (isInTimeout) {
            item = <div className='input-holder'><Loader className='loader' /></div>
        } else {
            item =
                <div className='input-holder'><input type='search'
                    placeholder='כתובת הIP של המצלמה' value={inputValue} onChange={(e) => setInputValue(e.target.value)} /></div>
        }
        return item
    }
    useEffect(() => {
        isInputValidCheck(inputValue)
    }, [inputValue])

    useEffect(() => {
        setIsInTimeout(false)
        setInputValue(props.ip)
    }, [props.bedId])
    useEffect(() => {
        setInputValue(props.ip)
    }, [props.ip])

    return (
        <div className='ip-dialog'>
            <div className='ip-dialog-title'>{(!isInTimeout) ? getTitleByPermission[getAuth()] : null}</div>
            <div className='main-window'>
                {getItem()}
            </div>
            <div className='controls'>
                {(props.dialogOpened) ? <div className={`button ${(isInputValid && !isInTimeout) ? 'enabled' : 'disabled'}`}
                    onClick={() => (isInputValid) ? onClick(0) : null} >{(appMode == applicationMode.REGULAR) ? Dictionary.calibrate : Dictionary.add}</div> : null}
                {(props.dialogOpened && isInputValid && isCalibSaved && props.appMode === applicationMode.REGULAR) ? <div className={`button-saved-calib`}
                    onClick={() => (isInputValid) ? onClick(1) : null} >{Dictionary.savedCalib}</div> : null}
            </div>
        </div>
    )
}
