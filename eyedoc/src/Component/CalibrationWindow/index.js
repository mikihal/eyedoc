import React, { useState, useEffect } from 'react';
import './CalibrationWindow.css'

const title = 'הוראות הכיול  :'
const calibrationInstructions = [
    1,
    2,
    3,
    4,
    5,
    6,
]

export default function CalibrationWindow() {

    const instructions = () => {
        return calibrationInstructions.map((instruction, i) => {
            return (
                <div key={i} className='instruction'>
                    {instruction}
                </div>
            )
        })
    }
    return (
        <div className='calibration-window video-place'>
            <div className='title'>{title}</div>
            <div className='instructions'>
                {instructions()}
            </div>
        </div>
    )
}