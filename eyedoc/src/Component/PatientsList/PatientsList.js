import React, { useState, useEffect } from 'react';
import './patientsList.css'
import Patient from '../Patient'
import NewPatient from '../NewPatient/NewPatient'
import { getAuth } from '../../Auth/AuthApi'
import SeverityLevel from '../../Types/dataLevelType';

const severityToNumber = {
    [SeverityLevel.NORMAL]: 0,
    [SeverityLevel.MEDIUM]: 1,
    [SeverityLevel.HIGH]: 2,
    [SeverityLevel.CRITICAL]: 3
}
let SeverityChanges = []
const PatientsList = (props) => {

    const [filterValue, setFilterValue] = useState("");
    const [isNewPatientOpen, setIsNewPatientOpen] = useState(false)
    // will hold for me the object for the current Severity and the time of update. the who watited longer will be shown first . {}
    const [patientesSeverity, setPatientsSeverity] = useState([]) // {bedId:string , severityLevel : dataTypeLevel(string) , updateTime : dateTime(miliseconds)}
    const [patientUnsavedChanges, setPatientUnsavedChanges] = useState(false);
    const [showUnsavedChangesAlert, setUnsavedChangesAlertVisibility] = useState(false);
    const userID = getAuth()

    useEffect(() => {
        setPatientUnsavedChanges(false)
        setUnsavedChangesAlertVisibility(false)
    }, [props.selectedPatient])

    const closeAddPatient = (np) => {
        setIsNewPatientOpen(false)
        props.savePatient(np)
    }

    const filterPatient = (patient) => {
        const filterValues = filterValue.trimLeft().trimRight().split(' ');

        if (patient.isDeleted) {
            return false;
        }

        const availableFilterValues = filterValues.filter((value) => {
            return (
                patient.firstName.includes(value) ||
                patient.lastName.includes(value) ||
                patient.bedId.toString().includes(value)
                // patient.id.toString().includes(value) search by ID number
            )
        })

        return filterValues.length == availableFilterValues.length
    }
    const sortItems = (x, y) => {
        let result = 0;
        if (!props.patientsData)
            return result;

        const xSeverityObject = patientesSeverity.find((severityObject) => x.bedId == severityObject.bedId);
        const ySeverityObject = patientesSeverity.find((severityObject) => y.bedId == severityObject.bedId);

        if (!ySeverityObject) {
            result = -1
        } else if (!xSeverityObject) {
            result = 1;
        } else {
            if (xSeverityObject.severityLevel == ySeverityObject.severityLevel) {
                result = xSeverityObject.severityUpdateTime < ySeverityObject.severityUpdateTime ? 1 : -1
            } else {
                result = severityToNumber[xSeverityObject.severityLevel] < severityToNumber[ySeverityObject.severityLevel] ? 1 : -1
            }
        }


        return result;
    }

    const updatePatientSeverity = (bedId, severityLevel, severityUpdateTime) => {
        SeverityChanges = [...SeverityChanges.filter((patientSeverityObject) => patientSeverityObject.bedId !== bedId)
            , { bedId: bedId, severityLevel: severityLevel, severityUpdateTime: severityUpdateTime }]

        setPatientsSeverity(SeverityChanges);
    }

    const removeFromPatientSevirityList = (bedId) => {
        SeverityChanges = [...SeverityChanges.filter((patientSeverityObject) => patientSeverityObject.bedId != bedId)]
        setPatientsSeverity(SeverityChanges);
    }

    const newPatientClick = () => {
        if (patientUnsavedChanges) {
            setUnsavedChangesAlertVisibility(true)
        } else {
            setIsNewPatientOpen(!isNewPatientOpen)
        }
    }

    const onPatientSelect = (patient)=>{
        props.setSelectedPatient(patient)
        setIsNewPatientOpen(false);
    }


    return (
        <div className='patients-list'>
            <div className='search-box'>
                <input type='search'
                    placeholder='חיפוש מטופל' value={filterValue} onChange={(e) => setFilterValue(e.target.value)}></input>
                {(userID == 1) ? <img className={`add ${(isNewPatientOpen) ? 'deselect' : ''}`}
                    onClick={newPatientClick}
                    src={require('../../Assets/Icons/plus.svg')}
                    alt='' /> : null}
            </div>
            <div className='add-patient-item'>
            </div>
            <div className='items-list'>
                {isNewPatientOpen ? <NewPatient getAllPatients={props.getAllPatients} closeWindow={setIsNewPatientOpen} closeAddPatient={(np) => closeAddPatient(np)}></NewPatient> : null}
                {props.patients
                    .filter((patient) => (filterPatient(patient)))
                    .sort((a, b) => sortItems(a, b))
                    .map((patient, i) => {
                        return (
                            <Patient
                                getAllPatients={props.getAllPatients}
                                // key is a must , to make the item unique and not get into troubles with borders as we did before.
                                key={patient.bedId}
                                onClick={() => onPatientSelect(patient) }
                                patient={patient}
                                enableTooltip={props.enableTooltip}
                                removeFromPatientSevirityList={removeFromPatientSevirityList}
                                updateTooltipCoords={props.updateTooltipCoords}
                                disableTooltip={props.disableTooltip}
                                patientData={props.patientsData.find((patientData) => patientData.id == patient.bedId)}
                                updatePatientSeverity={updatePatientSeverity}
                                className={((props.selectedPatient) && props.selectedPatient.bedId === patient.bedId) ? 'selected' : ''}
                                isDisabled={patientUnsavedChanges && !((props.selectedPatient) && props.selectedPatient.bedId === patient.bedId)}
                                showUnsavedChangesAlert={showUnsavedChangesAlert}
                                patientUnsavedChanges={patientUnsavedChanges}
                                setUnsavedChangesAlertVisibility={setUnsavedChangesAlertVisibility}
                                setPatientUnsavedChanges={setPatientUnsavedChanges}

                            />
                        )
                    })}
            </div>
        </div>
    )
}

export default PatientsList