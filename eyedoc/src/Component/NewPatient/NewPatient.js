import React, { useState, useEffect } from 'react';
import axios from 'axios'
import './newPatient.css'
import AlertDialog from '../AlertDialog/AlertDialog'
import ServerIp from '../../Config.js'
import { getAuth } from '../../Auth/AuthApi'

const dictionary = {
    bed: 'מיטה',
    patient: 'מטופל'
}

const NewPatient = (props) => {

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [age, setAge] = useState('')
    const [bed, setBed] = useState('')
    const [id, setId] = useState('')
    const [alertMessage, setAlertMessage] = useState(null)
    const userID = getAuth()
    const [ageError, setAgeError] = useState('')
    const [idError, setIdError] = useState('')
    const [validAge, setValidAge] = useState(true);
    const [validId, setValidId] = useState(true);
    // ("נא להכניס מס' הן 8-9 ספרות")

    const addPatient = () => {
        axios.post(`http://${ServerIp}/newPatient`,
            {
                firstName: firstName,
                lastName: lastName,
                age: age,
                bedId: bed,
                id,
                userId: userID
            }).then(response => {
                setAlertMessage(<AlertDialog onApprove={() => { setAlertMessage(null); props.closeWindow(false); props.getAllPatients() }} message={response.data.message}></AlertDialog>)

            })
    }

    const validateId = (e) => {
        let validation = /^[0-9]{8,9}$/;
        if (e.match(validation)) {
            setIdError('');
            setValidId(true);
            setId(e)
        }
        else {
            setIdError('נא להכניס מספר בן 9-8 ספרות');
            setValidId(false);
        }
    }

    const validateAge = (e) => {
        let validation = /^(?:1[01][0-9]|120|[1-9]|[1-9][0-9])$/;
        if (e.match(validation)) {
            setAgeError('')
            setValidAge(true)
            setAge(e)
        }
        else {
            setAgeError('נא להכניס גיל בין 1 ל 120');
            setValidAge(false)

        }
    }

    return (
        <div className='new-patient'>
            {alertMessage}
            <div className='title'>
                <span>הוספת מטופל חדש</span>
            </div>
            <div className='field'>
                <span className='name'>שם פרטי:</span>
                <input className='box' onChange={(e) => setFirstName(e.target.value)}></input>
            </div>
            <div className='field'>
                <span className='name'>שם משפחה:</span>
                <input className='box' onChange={(e) => setLastName(e.target.value)}></input>
            </div>
            <div className='field'>
                <span className='name'>תעודת זהות:</span>
                <input className='box' type='number' onChange={(e) => validateId(e.target.value)}></input>
            </div>
            <span className='validation'>{idError}</span>
            <div className='field'>
                <span className='name'>גיל:</span>
                <input className='box' type='number' min="1" max="120" onChange={(e) => validateAge(e.target.value)}></input>
            </div>
            <span className='validation'>{ageError}</span>

            <div className='field'>
                <span className='name'>מיטה:</span>
                <input className='box' onChange={(e) => setBed(e.target.value)}></input>
            </div>
            <div className='apply'>
                <img onClick={firstName != '' && lastName != '' && bed != '' && validAge && validId ? () => addPatient() : () => setAlertMessage(<AlertDialog onApprove={() => setAlertMessage(null)} message='יש למלא את כל השדות'></AlertDialog>)} src={require('../../Assets/Icons/plus.svg')}></img>
            </div>
        </div>
    )
}

export default NewPatient