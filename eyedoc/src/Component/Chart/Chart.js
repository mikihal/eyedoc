import React, { useState } from "react"
import { Label, Brush, LineChart, ResponsiveContainer, Line, AreaChart, YAxis, XAxis, Tooltip, Area, ReferenceLine, CartesianGrid } from 'recharts'
import moment from 'moment'
import './chart.css'

const Chart = (props) => {

    return (
        <ResponsiveContainer width='100%' height="100%">
            <LineChart data={props.data}
                margin={{ top: 20, right: 50, left: 0, bottom: 0 }}>
                <CartesianGrid strokeDasharray="5 5" stroke="#eee" />
                <XAxis
                    padding={{left: 0, right: 0}}
                    dataKey='time'
                    domain={['dataMin', 'dataMax']}
                    name='time'
                    tickFormatter={(unixTime) => moment(unixTime).format('HH:mm:ss')}
                    type='number'>
                    <Label value="→" stroke="white" offset={0} position="insideBottomLeft" />
                </XAxis>
                <YAxis />
                <ReferenceLine  y={props.medium} strokeWidth="0.5" stroke="orange" />
                <ReferenceLine  y={props.min} strokeWidth="0.5" stroke="red" />
                <ReferenceLine  y={props.max} strokeWidth="0.5" stroke="red" />
                <Line dot={false} type="natural" dataKey="value" stroke="cyan" fill="white" />
            </LineChart>
        </ResponsiveContainer>
    );
}

export default Chart

