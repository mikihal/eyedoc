import React, { useState, useEffect } from 'react';
import './Video.css'
import IPDialog from '../IPDialog';
import ServerIp from '../../Config'
import { getAuth } from '../../Auth/AuthApi';
import Permission from '../../Types/Permission';
import cameraMode from '../../Types/cameraMode';
import { useAppModeContext } from '../../Pages/Home';
import JSMpeg from 'jsmpeg-player'
import applicationMode from '../../Types/applicationMode';
import Axios from 'axios';
import Loader from '../../Assets/Svgs/Loader';
import ApplicationMode from '../../Types/applicationMode';

const SPINNER_TIMEOUT = 5000;
const dictionary = {
    dumbbellMachine: 'מכונת הנשמה',
    monitor: 'מוניטור',
    injectors: 'משאבת מזרק'
}
const cameraModeIcons = {
    [cameraMode.FULL]: 'fas fa-compress',
    [cameraMode.PARTIAL]: 'fas fa-expand'
}

const startCalibration = (clientId, cameraIp, cameraTargetId, isExists) => {
    Axios.get("http://" + ServerIp + "/connectCamera/" + clientId + "/" + cameraIp + "/" + cameraTargetId + "/" + isExists)
}

const deleteCamera = (bedId, cameraIndex) => {
    Axios.get("http://" + ServerIp + "/deleteCamera/" + bedId + "/" + cameraIndex)
}

const connectRTSPCamera = async (bedId, videoType) => {
    await Axios.get(`http://${ServerIp}/rtsp/connect/${bedId}/${videoType}`)
}
const disconnectRTSPCamera = async (videoType) => {
    await Axios.get(`http://${ServerIp}/rtsp/disconnect/${videoType}`)
}
const changeCameraIp = async (bedId, ip, videoType) => {
    await Axios.get(`http://${ServerIp}/rtsp/changeIp/${bedId}/${videoType}/${ip}`)
    await connectRTSPCamera(bedId, videoType);
}

export default function Video(props) {

    const [ipDialogOpened, setIPDialogOpen] = useState(false);
    const [isSpinnerVisable, setSpinnerVisability] = useState(false)

    const { appMode } = useAppModeContext()

    const getFunctionIcon = () => {
        let icon = null
        if (getAuth() == Permission.ADMIN) {
            let iconClassName = ''
            if (appMode === applicationMode.REGULAR) {
                iconClassName = `icon fas fa-plus ${(props.isVideoExist) ? 'delete' : 'add'}`
            } else {
                iconClassName = `icon fas fa-plus ${(props.rtspCameraConnected) ? 'delete' : 'add'}`
            }
            icon = <div className={iconClassName}
                onClick={() => {
                    props.getAllLastIp()
                    setIPDialogOpen(true);
                    (appMode === applicationMode.REGULAR) ?
                        deleteCamera(props.bedId, props.cameraIndex)
                        :
                        disconnectRTSPCamera(props.cameraIndex);
                }} />



        } else {
            icon = null
        }

        return icon;
    }

    const getVideoItem = () => {
        let videoItem;
        if (isSpinnerVisable) {
            videoItem = <Loader className='loader' />
        }
        else if (appMode === applicationMode.SLIM) {
            if (!props.rtspCameraConnected) {
                videoItem = <IPDialog appMode={appMode} bedId={props.bedId} cameraIndex={props.cameraIndex} dialogOpened={ipDialogOpened} ip={props.lastIp}
                    onSubmit={(appMode === applicationMode.REGULAR) ? startCalibration : changeCameraIp} />
            } else {
                return null
            }
        }
        else if (!props.isVideoExist) {
            videoItem = <IPDialog appMode={appMode} bedId={props.bedId} cameraIndex={props.cameraIndex} dialogOpened={ipDialogOpened} ip={props.lastIp} onSubmit={startCalibration} />
        } else {
            videoItem = (props.videoSrc.length) ? <img className='video-img' src={"data:image/jpg;base64," + props.videoSrc} /> :
                null
        }
        return videoItem
    }

    useEffect(() => {
        setIPDialogOpen(false)
    }, [props.bedId])

    useEffect(() => {
        const rtspPlayer = new JSMpeg.Player(`ws://localhost:999${props.cameraIndex}`, {
            canvas: document.getElementById(`rtspPlayerCanvas-${props.cameraIndex}`),
            "disableGl": true, // a flag that i need so i can destroy the player without causing excpetion because the canvas still alive..
        })

        return () => {
            if (rtspPlayer && rtspPlayer.source.socket)
                rtspPlayer.destroy()
        }
    }, [])


    useEffect(() => {
        let spinnerTimeout = null
        if (appMode === applicationMode.SLIM) {
            setSpinnerVisability(true)
            spinnerTimeout = setTimeout(() => {
                spinnerTimeout = null;
                setSpinnerVisability(false)
            }, SPINNER_TIMEOUT)
        }
        return () => {
            clearTimeout(spinnerTimeout)
            setSpinnerVisability(false)
        }
    }, [appMode, props.rtspCameraConnected])

    return (
        <div className={`video ${props.cameraMode}`}>
            <div className='video-title'>
                <div className='icons multi'>
                    {getFunctionIcon()}
                    {(appMode == ApplicationMode.REGULAR) ? <div className='icon fas fa-chart-line' onClick={props.changeDisplayMode} /> : null}
                </div>
                <div className='name'> {dictionary[props.videoName]}</div>
                <div className='icons one'>
                    <div className={`icon ${(cameraModeIcons[props.cameraMode])}`} onClick={() => props.changeSelectedCamera(props.videoName)}></div>
                </div>
            </div>
            <div className='video-place'>
                {getVideoItem()}
                <canvas id={`rtspPlayerCanvas-${props.cameraIndex}`}
                    className={`video-place rtsp-player ${(appMode === applicationMode.SLIM && props.rtspCameraConnected && !isSpinnerVisable) ? 'enabled' : 'disabled'} ${props.cameraMode}`} />
            </div>
        </div >
    )
}