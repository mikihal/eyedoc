import React, { useState, useEffect } from 'react'
import Chart from '../Chart/Chart.js'
import TimeRangeSlider from 'react-time-range-slider';
import DisplayMode from '../../Types/DisplayMode'
import './analysis.css'
import moment from 'moment'
import axios from 'axios';
import ServerIp from '../../Config'
import Pause from '../../Assets/Svgs/Pause.js';
import Play from '../../Assets/Svgs/Play.js';
import DataType from '../../Types/dataType.js';
import { dataType } from '../Patient/index.js';

const KEYS = [
    [
        'SBP',
        'MAP',
        'HRV',
        'CO2',
        'TEMP',
        'SAT'
    ],
    ['VTE', 'BR'],
    ['IR']
]

const SELECT_RANGE = "בחר טווח זמן לצפייה"

const Analysis = (props) => {

    const [chart, setChart] = useState(null)
    const [min, setMin] = useState(null)
    const [max, setMax] = useState(null)
    const [medium, setMedium] = useState(null)
    const [chartData, setChartData] = useState([])
    const [filteredData, setFilteredData] = useState(null)
    const [chartsOptions, setChartsOptions] = useState(KEYS[props.cameraIndex - 1])
    const [isRealTime, setIsRealTime] = useState(true)

    useEffect(() => {
        setChart(chartsOptions[0])
    }, [chartsOptions])


    const timeOptions = [
        {
            value: 1,
            desc: 'דקה'
        },
        {
            value: 30,
            desc: 'חצי שעה'
        },
        {
            value: 60,
            desc: 'שעה'
        },
        {
            value: 120,
            desc: 'שעתיים'
        }
    ]

    const [selectedTime, setSlectedTime] = useState(timeOptions[0].value)

    useEffect(() => {
        setChartData([])
        setMin(null)
        setMax(null)
        setFilteredData(null)
        let measure = dataType.BREATH_RATE
        switch (chart) {
            case 'MAP':
                setMax(null)
                setMin(null)
                setMedium(null)
                measure = DataType.AVERAGE_PRESSURE
                break
            case 'SBP':
                setMax(props.severityData.maxSBP)
                setMin(props.severityData.minSBP)
                setMedium(props.severityData.mediumSBP)
                measure = DataType.BLOOD_PRESSURE
                break
            case 'HRV':
                setMax(props.severityData.maxHRV)
                setMin(props.severityData.minHRV)
                setMedium(null)
                measure = DataType.PULSE
                break
            case 'CO2':
                setMax(null)
                setMin(props.severityData.minCO2)
                setMedium(null)
                measure = DataType.CO2
                break;
            case 'SAT':
                setMax(null)
                setMin(props.severityData.minSAT)
                setMedium(null)
                measure = DataType.SATORATION
                break
            case 'TEMP':
                setMax(props.severityData.maxTEMP)
                setMin(props.severityData.minTEMP)
                setMedium(props.severityData.mediumTEMP)
                measure = DataType.TEMP
                break;
            case 'BR':
                setMax(props.severityData.maxBR)
                setMin(null)
                setMedium(props.severityData.mediumBR)
                measure = DataType.BREATH_RATE
                break
            case 'IR':
                setMax(null)
                setMin(null)
                setMedium(null)
                measure = DataType.INJECTOR_RATE
                break
            case 'VTE':
                setMax(null)
                setMin(null)
                setMedium(null)
                measure = DataType.VTE
                break;
            default: {
                setMax(null)
                setMin(null)
                setMedium(null)
                measure = DataType.BREATH_RATE
                break
            }
        }
        if (props.displayMode == DisplayMode.USER_HISTORY) {
            axios.get(`http://${ServerIp}/stats/analysis/${props.bedId}/${props.cameraIndex}/${measure}/${selectedTime}`).then((res) => {
                setChartData(JSON.parse(res.data))
            })
            const chartDataIterval = isRealTime ? setInterval(() => {
                axios.get(`http://${ServerIp}/stats/analysis/${props.bedId}/${props.cameraIndex}/${measure}/${selectedTime}`).then((res) => {
                    setChartData(JSON.parse(res.data))
                })
            }, 3000) : null
            return () => { clearInterval(chartDataIterval); setChartData([]) }
        }

    }, [chart, props.bedId, props.displayMode, selectedTime, isRealTime])

    const [mindate, setMindate] = useState(new Date())
    const [maxdate, setMaxdate] = useState(new Date())

    const [startTime, setStartTime] = useState(mindate)
    const [endTime, setEndTime] = useState(maxdate)

    const [time, setTime] = useState({
        start: "0:00",
        end: "2:00"
    })

    useEffect(() => {
        setStartTime(mindate + (2000 * 60 * 60 * parseInt(time.start.substring(0, time.start.indexOf(":")))) + (2000 * 60 * parseInt(time.start.substring(time.start.indexOf(":") + 1))))
        setEndTime(mindate + (2000 * 60 * 60 * parseInt(time.end.substring(0, time.end.indexOf(":")))) + (2000 * 60 * parseInt(time.end.substring(time.end.indexOf(":") + 1))))
    }, [mindate, maxdate, time])

    useEffect(() => {
        if (chartData.length > 0) {
            setFilteredData(
                chartData.filter((data) => moment(data.time).isSameOrAfter(startTime) && moment(data.time).isSameOrBefore(endTime))
            )
        }
    }, [startTime, endTime, chartData])

    useEffect(() => {
        if (chartData.length > 0) {
            setMindate(chartData[0].time)
            setMaxdate(chartData[chartData.length - 1].time)
            setTime({
                start: "00:00",
                end: Math.round(Math.abs(chartData[chartData.length - 1].time - chartData[0].time) / 36e5) + ":" + Math.round((((chartData[chartData.length - 1].time - chartData[0].time) % 86400000) % 3600000) / 60000)
            })
        }
    }, [chartData])

    const chartsOptionsSelector = chartsOptions.map((option, i) => {
        return <button className={option == chart ? 'selected' : ''} key={i} onClick={() => option != chart ? setChart(option) : null}>{option}</button>
    })

    const timeOptionsScale = () => {
        return timeOptions.map((option, index) => {
            return (<div className='scale-section'>
                <div className='dot-holder'>
                    <div className={option.value <= selectedTime ? 'dot selected' : 'dot'}
                        onClick={() => setSlectedTime(option.value)}>
                        <span className='desc'>{option.desc}</span>
                    </div>
                </div>
                {index < timeOptions.length - 1 ? <div className={option.value < selectedTime ? 'line selected' : 'line'}></div> : null}
            </div>
            );
        })
    }

    return (
        <div className='chart-control'>
            {chartData.length > 0 ? <div className='current-value'>
                {chartData[chartData.length - 1].value}
            </div> : null}
            <div className='measure-selector'>
                {chartsOptionsSelector}
            </div>
            <div className='controls'>
                <Play className={isRealTime ? 'selected' : ''} onClick={() => setIsRealTime(true)} />
                <Pause className={isRealTime ? '' : 'selected'} onClick={() => setIsRealTime(false)} />
            </div>
            <div className='chart-wrapper'>
                {chart != null ? <Chart max={max} min={min} medium={medium} data={chartData}></Chart> : null}
            </div>
            <div className='time-range-wrapper'>
                <div className='time-range-slider'>
                    <div className='scale'>
                        {timeOptionsScale()}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Analysis