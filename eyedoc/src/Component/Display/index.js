import React, { useState, useEffect } from 'react'
import './Display.css'
import DisplayMode from '../../Types/DisplayMode'
import Video from '../Video'
import UserHistory from '../UserHistory'
import { useAppModeContext } from '../../Pages/Home'
import ApplicationMode from '../../Types/applicationMode'

export default function Display(props) {
    /**
     * this item is the holder of 2 other items
     * -- videoItem
     * -- userData
     */
    const [displayMode, setDisplayMode] = useState(DisplayMode.VIDEO)
    const { appMode } = useAppModeContext();

    // camera mode need to be changed to displayMode

    useEffect(() => {
        if (appMode === ApplicationMode.SLIM) {
            setDisplayMode(DisplayMode.VIDEO)
        }
    }, [appMode])

    useEffect(() => {
        setDisplayMode(DisplayMode.VIDEO)
    }, [props.bedId])
    return (
        <div className={`stream-view-item display ${props.className} ${props.cameraMode} ${displayMode}`}>
            <div className='holder'>
                <Video
                    displayMode={displayMode}
                    bedId={props.bedId}
                    isVideoExist={props.isVideoExist}
                    videoSrc={props.videoSrc}
                    cameraIndex={props.cameraIndex}
                    videoName={props.videoName}
                    cameraMode={props.cameraMode}
                    changeSelectedCamera={props.changeSelectedCamera}
                    rtspCameraConnected={props.rtspCameraConnected}
                    lastIp={props.lastIp}
                    changeDisplayMode={() => setDisplayMode(DisplayMode.USER_HISTORY)}
                    getAllLastIp={props.getAllLastIp}
                />
                <UserHistory
                    displayMode={displayMode}
                    bedId={props.bedId}
                    isVideoExist={props.isVideoExist}
                    videoSrc={props.videoSrc}
                    cameraIndex={props.cameraIndex}
                    severityData={props.severityData}
                    videoName={props.videoName}
                    cameraMode={props.cameraMode}
                    changeSelectedCamera={props.changeSelectedCamera}
                    rtspCameraConnected={props.rtspCameraConnected}
                    lastIp={props.lastIp}
                    changeDisplayMode={() => setDisplayMode(DisplayMode.VIDEO)}
                />
            </div>
        </div >
    )
}