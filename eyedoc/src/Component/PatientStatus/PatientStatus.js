import React, { useState, useEffect } from 'react';
import axios from 'axios'
import SeverityLevel from '../../Types/dataLevelType';
import './patientStatus.css'
import BloodPressure from '../../Assets/Svgs/BloodPressure';
import HeartBeat from '../../Assets/Svgs/HeartBeat';
import dataType from '../../Types/dataType';
import AlertDialog from '../AlertDialog/AlertDialog'
import ServerIp from '../../Config'
import Thresholds from '../Thresholds/Thresholds';
import reasons from '../../Types/reasons';
import { getAuth } from '../../Auth/AuthApi';
import stopBubbling from '../../Functions/StopBubbling';
import Question from '../../Assets/Svgs/Question';
import { useAppModeContext } from '../../Pages/Home';
import applicationMode from '../../Types/applicationMode';
import SeverityTime from '../SeverityTime';
import severityDataType from '../../Types/severityDataType';
import BreathRate from '../../Assets/Svgs/BreathRate';
import PatientDataItem from '../PatientDataItem'

export { ServerIp, dataType }

const NO_STATS = '---'
const SEVERITY_UPDATE_TIMEOUT = 5000; // 5s

const dictionary = {
    bed: 'מיטה',
    patient: 'מטופל'
}
const iconsNames = {
    [dataType.BLOOD_PRESSURE]: 'BP',
    [dataType.PULSE]: 'HRV',
    [dataType.BREATH_RATE]: 'BR',
}
const severityDictionary = {
    [SeverityLevel.CRITICAL]: 'קריטי',
    [SeverityLevel.HIGH]: 'קשה',
    [SeverityLevel.MEDIUM]: 'בינוני',
    [SeverityLevel.NORMAL]: 'יציב',
}
const Patient = (props) => {

    const [id, setId] = useState(props.patient.id)
    const [age, setAge] = useState(props.patient.age)
    const [firstname, setFirstname] = useState(props.patient.firstName)
    const [lastname, setLastname] = useState(props.patient.lastName)
    const [alertMessage, setAlertMessage] = useState(null)
    const [isThresholdsEditOpen, setIsThresholdsEditOpen] = useState(false)
    const [updatedThresholds, setUpdatedThresholds] = useState({})
    const [currntTime, setCurrntTime] = useState(Date.now())
    // the current selected severity
    const [currentSeverityLevel, setCurrentSeverityLevel] = useState(null)
    // the reason for the currrent severity
    const [currentSeverityReason, setCurrentSeverityReason] = useState(null)
    // the time the current severity was updated 
    const [severityUpdateTime, setSeverityUpdateTime] = useState(null)
    // the time the server send a different severity, it need to keep that severity for SEVERITY_UPDATE_TIMEOUT to make it the current severity
    const [updateSeverityRequestTime, setUpdateSeverityRequestTime] = useState(null)

    const userID = getAuth()
    const { appMode } = useAppModeContext()

    const tick = () => {
        setCurrntTime(Date.now())
    }

    useEffect(() => {
        const interval = setInterval(tick, 1000)
        return () => clearInterval(interval);
    }, [])

    useEffect(() => {
        if ((props.patientData && props.patientData.severity && props.patientData.severity.level && props.patientData.severity.lastUpdateTime)) {
            let requestTime = new Date(props.patientData.severity.lastUpdateTime)
            requestTime.setHours(requestTime.getHours() - 3);

            if (!currentSeverityLevel) {
                setCurrentSeverityLevel(props.patientData.severity.level)
                setCurrentSeverityReason(props.patientData.severity.reason)
                setUpdateSeverityRequestTime(requestTime.getTime())
                setSeverityUpdateTime(requestTime.getTime())

                props.updatePatientSeverity(props.patient.bedId, props.patientData.severity.level, requestTime.getTime())
            }
            else {
                if (currentSeverityLevel === props.patientData.severity.level) {
                    // can change reasons on the same status without waiting 
                    if (currentSeverityReason !== props.patientData.severity.reason) {
                        setCurrentSeverityReason(props.patientData.severity.reason)
                    }
                }
                else if (updateSeverityRequestTime !== requestTime.getTime()) {
                    setUpdateSeverityRequestTime(requestTime.getTime())
                }
                // the patient kept his status for SEVERITY_UPDATE_TIMEOUT seconds, its time for an update!
                else if (Math.ceil(currntTime - updateSeverityRequestTime) > SEVERITY_UPDATE_TIMEOUT) {
                    setCurrentSeverityLevel(props.patientData.severity.level)
                    setCurrentSeverityReason(props.patientData.severity.reason)
                    setSeverityUpdateTime(updateSeverityRequestTime);

                    props.updatePatientSeverity(props.patient.bedId, props.patientData.severity.level, updateSeverityRequestTime)
                }
            }
        }
        else {
            props.removeFromPatientSevirityList(props.patient.bedId)
            setCurrentSeverityLevel(null)
            setCurrentSeverityReason(reasons[6])
            setUpdateSeverityRequestTime(null)
            setSeverityUpdateTime(null)
        }
    }, [props.patientData])

    const getSeverityMessage = (currentSeverityReason) => {
        let message = ""
        let isHRVBigger;
        try {
            switch (currentSeverityReason) {
                case 0:
                    message = reasons[0]
                    break;
                case 1:
                    message = `לחץ דם סיסטולי נמוך מערך הסף שהוצב למטופל (${props.patient.severityData.minSBP}) 
                     סטורציה נמוכה מערך הסף שהוצב למטופל (${props.patient.severityData.minSAT})`
                    break;
                case 2:
                    isHRVBigger = (props.patientData.stats[dataType.PULSE].value > props.patient.severityData.maxHRV)
                    message = `סטורציה נמוכה מערך הסף שהוצב למטופל (${props.patient.severityData.minSAT}) 
                     קצב נשימתי גבוה מהערך הסף שהוצב למטופל (${props.patient.severityData.mediumBR}) 
                      דופק ${isHRVBigger ? 'גבוה' : 'נמוך'} מערך הסף שהוצב למטופל (${props.patient.severityData[isHRVBigger ? 'maxHRV' : 'minHRV']}) `
                    break;
                case 3:
                    isHRVBigger = (props.patientData.stats[dataType.PULSE].value > props.patient.severityData.maxHRV)
                    message = `לחץ דם סיסטולי נמוך מערך הסף שהוצב למטופל (${props.patient.severityData.minSBP}) 
                     דופק ${isHRVBigger ? 'גבוה' : 'נמוך'} מערך הסף שהוצב( ${props.patient.severityData[isHRVBigger ? 'maxHRV' : 'minHRV']})`
                    break;
                case 4:
                    message = `לחץ דם סיסטולי נמוך מערך הסף שהוצב למטופל (${props.patient.severityData.minSBP}) 
                     קצב נשימתי גבוה מערך הסף שהוצב למטופל (${props.patient.severityData.mediumBR})`
                    break;
                case 5:
                    message = `לחץ דם סיסטולי נמוך מערך הסף שהוצב למטופל (${props.patient.severityData.minSBP})`
                    break;
                case 6:
                    message = `סטורציה נמוכה מערך הסף שהוצב למטופל (${props.patient.severityData.minSAT})`
                    break;
                case 7:
                    message = `קצב נשימתי גבוה מערך הסף שהוצב למטופל (${props.patient.severityData.mediumBR})`
                    break;
                case 8:
                    isHRVBigger = (props.patientData.stats[dataType.PULSE].value > props.patient.severityData.maxHRV)
                    message = `דופק ${isHRVBigger ? 'גבוה' : 'נמוך'} מערך הסף שהוצב (${props.patient.severityData[isHRVBigger ? 'maxHRV' : 'minHRV']})`
                    break;
                case 9:
                    message = reasons[9]
                    break;
                default:
                    break;
            }
        } catch (e) {
            message = ""
        }
        return message
    }

    return (
        <div
            className={`patient-status border-${currentSeverityLevel}`}>
            {/* <div className='patient-status-details'> */}
            <div className='title'>
                {`${dictionary.bed} ${props.patient.bedId} - ${props.patient.firstName} ${props.patient.lastName}`}
            </div>
            <div className={`label status-label-${currentSeverityLevel}`}>
                {severityDictionary[currentSeverityLevel] ? severityDictionary[currentSeverityLevel] : "---"}
            </div>
            <div className='items-holder'>
                <PatientDataItem
                    itemName='blood_pressure'
                    itemData={(props.patientData && props.patientData.stats) ? props.patientData.stats['blood_pressure'] : null}
                ></PatientDataItem>
                <PatientDataItem
                    itemName='pulse'
                    itemData={(props.patientData && props.patientData.stats) ? props.patientData.stats['pulse'] : null}
                ></PatientDataItem>
                <PatientDataItem
                    itemName='breath_rate'
                    itemData={(props.patientData && props.patientData.stats) ? props.patientData.stats['breath_rate'] : null}
                ></PatientDataItem>
                <PatientDataItem
                    itemName='saturation'
                    itemData={(props.patientData && props.patientData.stats) ? props.patientData.stats['saturation'] : null}
                ></PatientDataItem>
                <PatientDataItem
                    itemName='injectorRate'
                    itemData={(props.patientData && props.patientData.stats) ? props.patientData.stats['injectorRate'] : null}
                ></PatientDataItem>
            </div>
            <div className='severity-info'>
                <div className='severity-desc'>
                    {props.patientData && props.patientData.severity ? getSeverityMessage(props.patientData.severity.reason) : getSeverityMessage(9)}
                </div>
                <div className='last-update'>
                    {(currentSeverityLevel && severityUpdateTime) ?
                        <SeverityTime severityLevel={currentSeverityLevel} currentTime={currntTime} severityUpdateTime={severityUpdateTime}
                            onMouseEnter={() => null}
                            onMouseLeave={() => null}
                            onMouseMove={() => null}
                        />
                        :
                        null
                    }
                </div>
            </div>
        </div >
    )
}

export default Patient