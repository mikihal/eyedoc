import React, { useState, useEffect } from 'react'
import './PatientFinalStatusItem.css'
import stopBubbling from '../../Functions/StopBubbling'
import SeverityLevel from '../../Types/dataLevelType'


const Dictionary = {
    [SeverityLevel.CRITICAL]: 'מצב קריטי',
    [SeverityLevel.HIGH]: 'מצב קשה',
    [SeverityLevel.MEDIUM]: 'מצב בינוני',
    [SeverityLevel.NORMAL]: 'נורמאלי',
    UNKNOWN: 'לא ניתן לדעת'
}

export default function PatientFinalStatusItem(props) {

    const onMouseEnter = (e) => {
        stopBubbling(e)
        props.enableTooltip({
            x: e.clientX,
            y: e.clientY
        }, props.message)
    }
    const onMouseLeave = (e) => {
        stopBubbling(e)
        props.disableTooltip()
    }
    const onMouseMove = (e) => {
        stopBubbling(e)
        props.updateTooltipCoords({
            x: e.clientX,
            y: e.clientY
        })
    }


    return (
        <div className={`patient-data-item final-status`}>
            <div
                className={`icon fas fa-question-circle ${(props.severityLevel) ? props.severityLevel : SeverityLevel.NORMAL}`}
                onMouseEnter={onMouseEnter}
                onMouseLeave={onMouseLeave}
                onMouseMove={onMouseMove}
            ></div>
            <div className={`severity-desc ${(props.severityLevel) ? props.severityLevel : SeverityLevel.NORMAL}`}>
                {Dictionary[props.severityLevel ? props.severityLevel : 'UNKNOWN']}
            </div>
        </div>
    )

}