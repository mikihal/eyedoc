import React, { useState, useEffect } from 'react';
import './SeverityTime.css'

const POINT_MOVE_TRIGGER = 60*60*1000; // 1h
const NUMBER_OF_POINTS = 15 

//getting from props, the current time and the severityTime
export default function SeverityTime(props) {

    const pointsList = () => {
        const dots = []
        const dotsToPaint = Math.ceil((props.currentTime - props.severityUpdateTime) / POINT_MOVE_TRIGGER)
        for (let i = 0; i < NUMBER_OF_POINTS; i++) {
            dots[i] = <div className={`severity-dot fas fa-circle ${(i < dotsToPaint) ? 'paint' : ''}`} />
        }
        return dots;
    }

    return (
        <div className={`severity-display ${props.severityLevel}`}
        onMouseEnter={(e) => props.onMouseEnter(e)}
        onMouseLeave={(e) => props.onMouseLeave(e)}
        onMouseMove={(e) => props.onMouseMove(e)}
        >
            {pointsList()}
        </div>
    )
}