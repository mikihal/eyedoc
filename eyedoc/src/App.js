import React, { useState, useEffect } from 'react';
import axios from 'axios'
import Home from './Pages/Home'
import Auth from './Pages/Auth'
import ServerIp from './Config.js'
import { useAuthContext, AuthProvider } from './Auth/AuthApi'
import './App.css';
import '@fortawesome/fontawesome-free/css/all.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useHistory
} from "react-router-dom";

export default function App() {

const ProtectedRoute = ({ component: Component, ...rest }) => {
  const { auth } = useAuthContext()
  return (
    <Route
      {...rest}
      render={() => auth ? (
        <Component />
      ) : (
          <Redirect to='/login' />
        )}
    />
  )
}
  return (
    <Router>
      <AuthProvider>
        <div className='App'>
          <Switch>
            <Route path='/login' component={Auth} />
            <ProtectedRoute path='/' component={Home} />
          </Switch>
        </div>
      </AuthProvider>
    </Router>
  )
}
