const THRESHOLDS_KEYS = {
    "minSBP": "SBP נמוך",
    "mediumSBP" : 'SBP בינוני',
    "maxSBP": "SBP גבוה",
    "minDBP": "DBP נמוך",
    "mediumDBP" : 'DBP בינוני',
    "maxDBP": "DBP גבוה",
    "maxHRV": "HRV גבוה",
    "minHRV": "HRV נמוך",
    "maxPR": 'PR גבוה',
    "minPR": 'PR נמוך',
    "minCO2": "CO2 נמוך",
    "maxTEMP": "TEMP גבוה",
    "minTEMP": "TEMP נמוך",
    "mediumTEMP": "TEMP בינוני",
    "minSAT": "SAT נמוך",
    "mediumBR": "BR בינוני",
    "maxBR": "BR גבוה"
}
export default THRESHOLDS_KEYS