const Cameras = {
    NONE : 'none',
    MONITOR: 'monitor',
    DUMBBELL_MACHINE: 'dumbbellMachine',
    INJECTORS: 'injectors',
    PATIENT_DETAILS : 'patient_details'
}

export default Cameras;