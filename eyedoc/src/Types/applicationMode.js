const ApplicationMode = {
    REGULAR: 'regular',
    SLIM: 'slim'
}
export default ApplicationMode;
