const CameraMode = {
    PARTIAL : 'partial',
    FULL : 'full'
}
export default CameraMode;