const DataType = {
    BREATH_RATE: 'breath_rate',
    PULSE: 'pulse',
    SATORATION: 'saturation',
    AVERAGE_PRESSURE: 'average_blood_pressure',
    BLOOD_PRESSURE: 'blood_pressure',
    INJECTOR_RATE: 'injectorRate',
    TEMP: 'temp',
    CO2 : 'co2',
    VTE: 'vte'
}
export default DataType