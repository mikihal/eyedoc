
const SeverityDataType = {
    GROWN_PERSON: 'grownPerson',
    LOW_PULSE: 'lowPulse',
    HIGH_PULSE: 'highPulse',
    LOW_BLOOD: 'lowBlood',
    MEDIUM_BLOOD: 'mediumBlood',
    LOW_BREATH: 'lowBreath',
    MEDIUM_BREATH: 'mediumBreath',
    HIGH_BREATH: 'highBreath',
    LOW_AVERAGE_PRESSURE: 'lowAveragePressure',
    LOW_STURATION: 'lowSturation',
    MEDIUM_SATURATION: 'mediumSturation'

}
export default SeverityDataType