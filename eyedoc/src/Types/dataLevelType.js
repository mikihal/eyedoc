const SeverityLevel = {
    CRITICAL: "critical",
    HIGH: "high",
    MEDIUM: "medium",
    NORMAL: "normal"
}
export default SeverityLevel