import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios'
import './Home.css';
import StreamsView from '../StreamsView';
import '@fortawesome/fontawesome-free/css/all.css'
import PatientsList from '../../Component/PatientsList/PatientsList'
import ServerIp from '../../Config'
import { getAuth, logOut } from '../../Auth/AuthApi'
import Tooltip from '../../Component/Tooltip/Tooltip';
import applicationMode from '../../Types/applicationMode';
import Loader from '../../Assets/Svgs/Loader';
import Statuses from '../Statuses';

const AppModeContext = React.createContext();
export const useAppModeContext = () => useContext(AppModeContext)

const Dictionary = {
  exit: 'התנתק',
  [applicationMode.REGULAR]: 'מלא',
  [applicationMode.SLIM]: 'מופחת',
  [applicationMode.STATUS]: 'כללי'
}
export default function Home() {

  const [appMode, setAppMode] = useState(applicationMode.REGULAR);
  const auth = getAuth()
  const [patients, setPatients] = useState([])
  const [patientsData, setPatientsData] = useState([])
  const [selectedPatient, setSelectedPatient] = useState(null)
  const [tooltipVisability, setTooltipVisability] = useState(false)
  const [tooltipMessage, setTooltipMessage] = useState(null)
  const [tooltipUserCoords, setTooltipUserCoords] = useState(null)

  useEffect(() => {
    getAllPatients()
  }, [])
 
  useEffect(() => {
    const getAllStatsInterval = (appMode === applicationMode.REGULAR || appMode === applicationMode.STATUS) ? setInterval(getAllStats, 1000) : null
    return () => (getAllStatsInterval) ? clearInterval(getAllStatsInterval) : null;
  }, [appMode])

  const getAllPatients = () => {
    axios.get(`http://${ServerIp}/patients?auth=${auth}`).then((response) => {
      setPatients(JSON.parse(JSON.parse(JSON.stringify(response.data.patients))))
    })
  }

  const requestAllStats = async () => {
    let response = await axios.get(`http://${ServerIp}/allStats?auth=${auth}`)
    return response.data
  }
  const getAllStats = async () => {
    setPatientsData(await requestAllStats())
  }
  const enableTooltip = (userCoords, message) => {
    setTooltipVisability(true);
    setTooltipUserCoords(userCoords);
    setTooltipMessage(message);
  }
  const updateTooltipCoords = (userCoords) => {
    setTooltipUserCoords(userCoords);
  }
  const disableTooltip = () => {
    setTooltipVisability(false);
    setTooltipUserCoords(null);
    setTooltipMessage(null);
  }

  useEffect(() => {
    let newSelectedPatient;
    if (patients.length) {
      if (selectedPatient && patients.find((patient) => patient.bedId == selectedPatient.bedId)) {
        newSelectedPatient = selectedPatient
      } else {
        newSelectedPatient = patients[0]
      }
    } else {
      newSelectedPatient = null
    }
    setSelectedPatient(newSelectedPatient)
  }, [patients])



  return (
    <AppModeContext.Provider value={{ appMode, setAppMode }}>
      <div className={`Home ${appMode === Dictionary.STATUS ? 'status' : 'regular'}`}>
        <img className='system-logo' src={require('../../Assets/Icons/logo.png')}></img>
        {/* <img className='lidao-logo' src={require('../../Assets/Icons/LaniadoIcon.svg')}></img> */}
        <img className='mafat-logo' src={require('../../Assets/Icons/mafat.png')}></img>
        <img className='mantak-logo' src={require('../../Assets/Icons/Mantak.png')} />
        <Loader className='loader' />
        <div className='exit-button' onClick={logOut}>{Dictionary.exit}</div>
        <div className='toggle'>
          <div className={`option ${(appMode === applicationMode.REGULAR) ? 'enabled' : 'disabled'}`}
            onClick={() => setAppMode(applicationMode.REGULAR)}>{Dictionary[applicationMode.REGULAR]}</div>
          <div className={`option ${(appMode === applicationMode.SLIM) ? 'enabled' : 'disabled'}`}
            onClick={() => setAppMode(applicationMode.SLIM)}>{Dictionary[applicationMode.SLIM]}</div>
          <div className={`option ${(appMode === applicationMode.STATUS) ? 'enabled' : 'disabled'}`}
            onClick={() => setAppMode(applicationMode.STATUS)}>{Dictionary[applicationMode.STATUS]}</div>
        </div>
        {appMode === Dictionary.STATUS ?
          <Statuses
            getAllPatients={getAllPatients} selectedPatient={selectedPatient}
            patients={patients}
            patientsData={patientsData}
            enableTooltip={enableTooltip}
            updateTooltipCoords={updateTooltipCoords}
            disableTooltip={disableTooltip}>
          </Statuses>
          : <PatientsList getAllPatients={getAllPatients} selectedPatient={selectedPatient}
            setSelectedPatient={setSelectedPatient}
            patients={patients}
            patientsData={patientsData}
            enableTooltip={enableTooltip}
            updateTooltipCoords={updateTooltipCoords}
            disableTooltip={disableTooltip}
          />}

        {appMode === Dictionary.STATUS ? null :
          <StreamsView
            patient={selectedPatient}
            enableTooltip={enableTooltip}
            updateTooltipCoords={updateTooltipCoords}
            disableTooltip={disableTooltip}
            patientData={(selectedPatient && patientsData) ? patientsData.find((data) => data.id == selectedPatient.bedId) : []} />}
        <Tooltip isVisable={tooltipVisability} tooltipMessage={tooltipMessage} userCoords={tooltipUserCoords} />
      </div>
    </AppModeContext.Provider>
  )
}