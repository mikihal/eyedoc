import React, { useState, useEffect } from 'react';
import './Auth.css';
import logoRapat from '../../Assets/Icons/rapat.png'
import logo from '../../Assets/Icons/logo.png'
import lindaoLogo from '../../Assets/Icons/lindao.png'
import modLogo from '../../Assets/Icons/MOD.png'
import MafatLogo from '../../Assets/Icons/mafat.png'
import axios from 'axios'
import { useAuthContext } from '../../Auth/AuthApi';
import ServerIp from '../../Config'
import stopBubbling from '../../Functions/StopBubbling';

const dictionary = {
  user: 'שם משתמש',
  password: 'סיסמה'
}
const ENTER_KEY = "Enter";
const ENTER_KEY_CODE = 13;

export default function Auth() {
  const [name, setName] = useState('')
  const [password, setPassword] = useState('')

  const { setAuth } = useAuthContext()

  const onSubmit = () => {
    axios.post('http://' + ServerIp + '/login',
      {
        name: name,
        password: password
      })
      .then(res => {
        if (res.data.token) {
          alert("התחברת בהצלחה");
          setAuth(res.data.token);
        }
        else {
          alert("שם משתמש או סיסמה לא נכונים");
        }
      })
      .catch(err => {
      })
  }

  const onKeyPress = (e) => {
    if (e.key == ENTER_KEY || e.code == ENTER_KEY || e.keyCode == ENTER_KEY_CODE) {
      stopBubbling(e)
      onSubmit()
    }
  }


  useEffect(() => {
    window.addEventListener("keypress", onKeyPress)
    return () => window.removeEventListener("keypress", onKeyPress)
  }, [name, password])

  return (
    <div className='Auth'>
      <div className='logos-box'>
        <img src={logoRapat} className=" logo Rapat"></img>
        <img src={modLogo} className="logo mod"></img>
        {/* <img src={lindaoLogo} className="logo lindao"></img> */}
        <img src={MafatLogo} className="logo mod"></img>
      </div>
      <div className="Title">
        <img src={logo} className="Logo"></img>
        <span>התחברות למערכת</span>
      </div>
      <div className="form">
        <div className='Card'>
          <label>{dictionary.user + ':'}</label>
          <input name="user" type="text" onChange={e => setName(e.target.value)} required />
          <label>{dictionary.password + ':'}</label>
          <input name="password " type="password" onChange={e => setPassword(e.target.value)} required />
        </div>
        <input onClick={onSubmit} type="submit" value="התחבר" className="Button"></input>
      </div>
    </div>
  )
}

