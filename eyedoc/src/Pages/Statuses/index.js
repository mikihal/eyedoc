import React, { useState, useEffect } from 'react'
import './statuses.css'
import PatientStatus from '../../Component/PatientStatus/PatientStatus'
import { getAuth } from '../../Auth/AuthApi'
import SeverityLevel from '../../Types/dataLevelType';

const severityToNumber = {
    [SeverityLevel.NORMAL]: 0,
    [SeverityLevel.MEDIUM]: 1,
    [SeverityLevel.HIGH]: 2,
    [SeverityLevel.CRITICAL]: 3
}
let SeverityChanges = []

const PATIENTS_IN_PAGE = 8
const TIME_PER_PAGE = 10

const Statuses = (props) => {

    const [filterValue, setFilterValue] = useState("");
    const [isNewPatientOpen, setIsNewPatientOpen] = useState(false)
    // will hold for me the object for the current Severity and the time of update. the who watited longer will be shown first . {}
    const [patientesSeverity, setPatientsSeverity] = useState([]) // {bedId:string , severityLevel : dataTypeLevel(string) , updateTime : dateTime(miliseconds)}
    const [patientUnsavedChanges, setPatientUnsavedChanges] = useState(false);
    const [showUnsavedChangesAlert, setUnsavedChangesAlertVisibility] = useState(false);
    const userID = getAuth()
    const [rangeStart, setRangeStart] = useState(0)
    const [timePerPage, setTimePerPage] = useState(TIME_PER_PAGE)

    useEffect(() => {
        setPatientUnsavedChanges(false)
        setUnsavedChangesAlertVisibility(false)
    }, [props.selectedPatient])

    const closeAddPatient = (np) => {
        setIsNewPatientOpen(false)
        props.savePatient(np)
    }

    const rangeInterval = () => {
        setTimeout(() => {
            if (rangeStart + PATIENTS_IN_PAGE < props.patients.length) {
                setRangeStart(rangeStart + PATIENTS_IN_PAGE)
            } else {
                setRangeStart(0)
            }
        }, timePerPage * 1000)
    }

    useEffect(() => {
        rangeInterval()
    }, [rangeStart])

    const filterPatient = (patient) => {
        const filterValues = filterValue.trimLeft().trimRight().split(' ');

        if (patient.isDeleted) {
            return false;
        }

        const availableFilterValues = filterValues.filter((value) => {
            return (
                patient.firstName.includes(value) ||
                patient.lastName.includes(value) ||
                patient.bedId.toString().includes(value)
                // patient.id.toString().includes(value) search by ID number
            )
        })

        return filterValues.length == availableFilterValues.length
    }
    const sortItems = (x, y) => {
        let result = 0;
        if (!props.patientsData)
            return result;

        const xSeverityObject = patientesSeverity.find((severityObject) => x.bedId == severityObject.bedId);
        const ySeverityObject = patientesSeverity.find((severityObject) => y.bedId == severityObject.bedId);

        if (!ySeverityObject) {
            result = -1
        } else if (!xSeverityObject) {
            result = 1;
        } else {
            if (xSeverityObject.severityLevel == ySeverityObject.severityLevel) {
                result = xSeverityObject.severityUpdateTime < ySeverityObject.severityUpdateTime ? 1 : -1
            } else {
                result = severityToNumber[xSeverityObject.severityLevel] < severityToNumber[ySeverityObject.severityLevel] ? 1 : -1
            }
        }


        return result;
    }

    const updatePatientSeverity = (bedId, severityLevel, severityUpdateTime) => {
        SeverityChanges = [...SeverityChanges.filter((patientSeverityObject) => patientSeverityObject.bedId !== bedId)
            , { bedId: bedId, severityLevel: severityLevel, severityUpdateTime: severityUpdateTime }]

        setPatientsSeverity(SeverityChanges);
    }

    const removeFromPatientSevirityList = (bedId) => {
        SeverityChanges = [...SeverityChanges.filter((patientSeverityObject) => patientSeverityObject.bedId != bedId)]
        setPatientsSeverity(SeverityChanges);
    }

    const newPatientClick = () => {
        if (patientUnsavedChanges) {
            setUnsavedChangesAlertVisibility(true)
        } else {
            setIsNewPatientOpen(!isNewPatientOpen)
        }
    }

    return (
        <div className='patients-statuses-list'>
            <div className='search-box'>
                <span>השהיה:</span>
                <input className='range' type='number' value={timePerPage} onChange={(e) => setTimePerPage(e.target.value)}></input>
                <span>שניות</span>
                <input type='search'
                    className='search'
                    placeholder='חיפוש מטופל' value={filterValue} onChange={(e) => setFilterValue(e.target.value)}></input>
                <span className='displays'>
                    מוצגים: {rangeStart + 1}-{rangeStart + PATIENTS_IN_PAGE > props.patients.length ? props.patients.length : rangeStart + PATIENTS_IN_PAGE}/{props.patients.length}
                </span>
            </div>
            <div className='items-list'>
                {props.patients
                    .filter((patient) => (filterPatient(patient)))
                    .sort((a, b) => sortItems(a, b))
                    .slice(rangeStart, rangeStart + PATIENTS_IN_PAGE > props.patients.length ? props.patients.length : rangeStart + PATIENTS_IN_PAGE)
                    .map((patient, i) => {
                        return (
                            <PatientStatus
                                getAllPatients={props.getAllPatients}
                                key={patient.bedId}
                                patient={patient}
                                enableTooltip={props.enableTooltip}
                                removeFromPatientSevirityList={removeFromPatientSevirityList}
                                updateTooltipCoords={props.updateTooltipCoords}
                                disableTooltip={props.disableTooltip}
                                patientData={props.patientsData.find((patientData) => patientData.id == patient.bedId)}
                                updatePatientSeverity={updatePatientSeverity}
                            />
                        )
                    })}
            </div>
        </div>
    );
}

export default Statuses