import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios'
import './StreamsView.css'
import ServerIp from '../../Config'
import PatientData from '../../Component/PatientData';
import SeverityLevel from '../../Types/dataLevelType'
import { useAppModeContext } from '../Home';
import applicationMode from '../../Types/applicationMode';
import cameras from '../../Types/cameras';
import cameraMode from '../../Types/cameraMode';
import Display from '../../Component/Display';
import AlertDialog from '../../Component/AlertDialog/AlertDialog';
import DataType from '../../Types/dataType';

const SEVERITY_UPDATE_TIMEOUT = 5000; // 5s
const STATS_SELECTION_KEY = 'statsSelection'

const dictionary = {
    bed: 'מיטה',
    patient: 'מטופל',
}

const cameraTypeDictionary = {
    [cameras.MONITOR]: 1,
    [cameras.DUMBBELL_MACHINE]: 2,
    [cameras.INJECTORS]: 3
}

const SelectedStatsContext = React.createContext();
export const useSelectedStatsContext = () => useContext(SelectedStatsContext)

export default function StreamsView(props) {

    const [video1, setVideo1] = useState("")
    const [video2, setVideo2] = useState("")
    const [video3, setVideo3] = useState("")
    const [rtsp1Connected, setRTSP1Connection] = useState(false);
    const [rtsp2Connected, setRTSP2Connection] = useState(false);
    const [rtsp3Connected, setRTSP3Connection] = useState(false);
    const { appMode } = useAppModeContext()
    const [selectedCamera, setSelectedCamera] = useState(cameras.NONE);
    // the current selected severity
    const [currentSeverityLevel, setCurrentSeverityLevel] = useState(null)
    // the time the server send a different severity, it need to keep that severity for SEVERITY_UPDATE_TIMEOUT to make it the current severity
    const [updateSeverityRequestTime, setUpdateSeverityRequestTime] = useState(null)
    const [currntTime, setCurrntTime] = useState(Date.now())
    const [ip1, setIp1] = useState("")
    const [ip2, setIp2] = useState("")
    const [ip3, setIp3] = useState("")
    const [selectedStats, setSelectedStats] = useState([]);
    const [alertMessage, setAlertMessage] = useState(null)




    const tick = () => {
        setCurrntTime(Date.now())
    }
    const getVideo = async (videoType) => {
        let response = await axios.get("http://" + ServerIp + "/image/" + props.patient.bedId + "/" + videoType)
        return response.data
    }
    const isRTSPCameraConnected = async (videoType) => {
        const response = await axios.get(`http://${ServerIp}/rtsp/status/${videoType}`)
        return response.data
    }
    const disconnectAllRTSPCameras = async () => {
        await axios.get(`http://${ServerIp}/rtsp/disconnectAll`)
    }
    const connectAllRTSPCameras = async () => {
        await axios.get(`http://${ServerIp}/rtsp/connectAll/${props.patient.bedId}`)
    }

    const getAllVideos = async () => {
        setVideo1(await getVideo(1))
        setVideo2(await getVideo(2))
        setVideo3(await getVideo(3))
    }
    const getAllRTSPCamerasConnection = async () => {
        setRTSP1Connection(await isRTSPCameraConnected(cameraTypeDictionary[cameras.MONITOR]))
        setRTSP2Connection(await isRTSPCameraConnected(cameraTypeDictionary[cameras.DUMBBELL_MACHINE]))
        setRTSP3Connection(await isRTSPCameraConnected(cameraTypeDictionary[cameras.INJECTORS]))
    }

    const getIp = async (videoType) => {
        let response = await axios.get(`http://${ServerIp}/getIP/${props.patient.bedId}/${videoType}`)
        return response.data
    }

    const getAllIp = async () => {
        setIp1(await getIp(1))
        setIp2(await getIp(2))
        setIp3(await getIp(3))
    }

    const changeSelectedCamera = (camera) => {
        setSelectedCamera((selectedCamera !== camera) ? camera : cameras.NONE)
    }

    const updateSelectedStats = (statKey) => {
        if (selectedStats.includes(statKey)) {
            if (selectedStats.length > 1) {
                setSelectedStats([...selectedStats.filter((key) => key !== statKey)])
            }
            else {
                setAlertMessage(<AlertDialog onApprove={() => { setAlertMessage(null) }} message={'לא ניתן לבחור פחות מנתון 1'} />)
            }
        } else {
            if (selectedStats.length < 9) {
                setSelectedStats([...selectedStats, statKey])
            } else {
                setAlertMessage(<AlertDialog onApprove={() => { setAlertMessage(null) }} message={'לא ניתן לבחור יותר מ-9 נתונים'} />)
            }
        }
    }
    const getDefaultSelection = () => {
        return Object.keys(DataType).slice(0, 9);
    }

    useEffect(() => {
        const interval = setInterval(tick, 1000)
        return () => clearInterval(interval)
    }, [])

    useEffect(() => {
        let statsSelection;
        try {
            statsSelection = JSON.parse(localStorage.getItem(STATS_SELECTION_KEY))
            if (!statsSelection) {
                statsSelection = getDefaultSelection();
            }
        } catch{
            statsSelection = getDefaultSelection();
        }
        setSelectedStats(statsSelection)
    }, [])

    useEffect(() => {
        if (selectedStats.length > 0)
            localStorage.setItem(STATS_SELECTION_KEY, JSON.stringify(selectedStats))
    }, [selectedStats])

    useEffect(() => {
        let videoInterval = null
        if (props.patient) {
            if (appMode === applicationMode.REGULAR) {
                getAllIp()
                getAllVideos()
                videoInterval = setInterval(getAllVideos, 1000)
            } else {
                connectAllRTSPCameras();
                getAllRTSPCamerasConnection()
                videoInterval = setInterval(getAllRTSPCamerasConnection, 1000)
            }
        }

        return () => {
            disconnectAllRTSPCameras()
            setVideo1("")
            setVideo2("")
            setVideo3("")
            setIp1("")
            setIp2("")
            setIp3("")
            setRTSP1Connection(false)
            setRTSP2Connection(false)
            setRTSP3Connection(false)
            if (videoInterval) clearInterval(videoInterval)
        }
    }, [props.patient, appMode])

    useEffect(() => {
        setCurrentSeverityLevel(null)
    }, [props.patient])

    useEffect(() => {
        setSelectedCamera(cameras.NONE)
    }, [props.patient])

    useEffect(() => {

        if ((props.patientData && props.patientData.severity && props.patientData.severity.level && props.patientData.severity.lastUpdateTime)) {
            let requestTime = new Date(props.patientData.severity.lastUpdateTime)
            requestTime.setHours(requestTime.getHours() - 3);

            if (!currentSeverityLevel) {
                setCurrentSeverityLevel(props.patientData.severity.level)
                setUpdateSeverityRequestTime(requestTime.getTime())
            }
            else if (currentSeverityLevel === props.patientData.severity.level) {
                // do nothing .you where you need to be.
            }
            // checking if its the same request for update as before
            else if (updateSeverityRequestTime !== requestTime.getTime()) {
                setUpdateSeverityRequestTime(requestTime.getTime())
            }
            // the patient kept his status for SEVERITY_UPDATE_TIMEOUT seconds, its time for an update!
            else if (Math.ceil(currntTime - updateSeverityRequestTime) > SEVERITY_UPDATE_TIMEOUT) {
                setCurrentSeverityLevel(props.patientData.severity.level)
            }
        } else {
            setCurrentSeverityLevel(null)
            setUpdateSeverityRequestTime(null)
        }
    }, [props.patientData])

    if (props.patient) {
        return (
            <SelectedStatsContext.Provider value={{ selectedStats, updateSelectedStats }}>
                <div className={`streams-view ${((appMode == applicationMode.REGULAR) && currentSeverityLevel) ? currentSeverityLevel : SeverityLevel.NORMAL}`}>
                    {alertMessage}
                    <div className='title'>{`${dictionary.bed} ${props.patient.bedId} - ${props.patient.firstName} ${props.patient.lastName}`}</div>
                    <div className={`items-holder ${selectedCamera !== cameras.NONE ? 'full' : 'partial'}`}>
                        {[
                            <PatientData
                                className={'top-right'}
                                cameraMode={(selectedCamera === cameras.PATIENT_DETAILS) ? cameraMode.FULL : cameraMode.PARTIAL}
                                patient={props.patient} patientData={props.patientData}
                                enableTooltip={props.enableTooltip}
                                updateTooltipCoords={props.updateTooltipCoords}
                                disableTooltip={props.disableTooltip}
                                changeSelectedCamera={changeSelectedCamera}
                                videoName={cameras.PATIENT_DETAILS}
                            />,
                            <Display
                                bedId={props.patient.bedId}
                                className={'top-left'}
                                isVideoExist={(video1 != "")}
                                videoSrc={video1}
                                cameraIndex={1}
                                severityData={props.patient.severityData}
                                videoName={cameras.MONITOR}
                                cameraMode={(selectedCamera === cameras.MONITOR) ? cameraMode.FULL : cameraMode.PARTIAL}
                                changeSelectedCamera={changeSelectedCamera}
                                rtspCameraConnected={rtsp1Connected}
                                lastIp={ip1}
                                getAllLastIp={getAllIp}
                            />,
                            <Display
                                bedId={props.patient.bedId}
                                className={'bottom-left'}
                                isVideoExist={(video2 != "")}
                                videoSrc={video2}
                                cameraIndex={2}
                                severityData={props.patient.severityData}
                                videoName={cameras.DUMBBELL_MACHINE}
                                cameraMode={(selectedCamera === cameras.DUMBBELL_MACHINE) ? cameraMode.FULL : cameraMode.PARTIAL}
                                changeSelectedCamera={changeSelectedCamera}
                                rtspCameraConnected={rtsp2Connected}
                                lastIp={ip2}
                                getAllLastIp={getAllIp}
                            />,
                            <Display
                                bedId={props.patient.bedId}
                                className={'bottom-right'}
                                isVideoExist={(video3 != "")}
                                videoSrc={video3}
                                cameraIndex={3}
                                severityData={props.patient.severityData}
                                videoName={cameras.INJECTORS}
                                cameraMode={(selectedCamera === cameras.INJECTORS) ? cameraMode.FULL : cameraMode.PARTIAL}
                                changeSelectedCamera={changeSelectedCamera}
                                rtspCameraConnected={rtsp3Connected}
                                lastIp={ip3}
                                getAllLastIp={getAllIp}
                            />
                        ]}
                    </div>
                </div>
            </SelectedStatsContext.Provider>
        )
    }
    else {
        return (
            <div className='streams-view empty'></div>
        )
    }
}